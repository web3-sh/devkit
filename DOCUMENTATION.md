#  | [![build status](https://gitlab.com/space-sh/devkit/badges/master/build.svg)](https://gitlab.com/space-sh/devkit/commits/master)


## /account/
	

+ list
+ seed
+ sign
+ transfer

## /add_args/
	Append arguments to existing binary 

	Run the compiler within the container to append
	constructor arguments to a contract in a specific project.
	Export SOLC and GETH variables to overwrite defaults.
	


## /build/
	Build the smart contracts to the blockchain(s) for a given environment

	Build all smart contracts as defined in the Dappfile.yaml to the blockchain(s) network(s)
	defined by the given environment.
	


## /compile/
	Compile a project's all smart contracts or a single one

	Compile all (updated) contracts for a project as defined in the Dappfile.yaml,
	or optionally only a single one.
	


## /console/
	Open a geth console attached to the container


## /convert/
	

+ toether
+ towei

## /enter/
	Open a shell inside the container

+ projects

## /events/
	Watch all events from one or more contracts, given a block range


## /get_receipt/
	Read a transaction receipt


## /info/
	Get info on the contracts for a project

	Show information about contracts and their deployed addresses.
	


## /init/
	Create a new DApp project from a template git repository

	Clones a git repository and removes the .git folder to uninitilize the repository.
	If no repository URI is given, then it copies the built-in dapp template.
	


## /js/
	Build the JavaScript representation of the contracts for a given environment

	Build the contracts.js according the the Dappfile.yaml
	defined by the given environment.
	


## /make/
	Compile, build and build js for project

	Run all three targets sequentially: /compile/, /build/ and /js/
	


## /run/
	Run Devkit container


## /stop/
	Stop Devkit container


## /test/
	Test a smart contract

	Run tests for one or more contracts inside the container for a specific project.
	


# Functions 

## DEVKIT\_DEP\_INSTALL()  
  
  
  
Check dependencies for this module.  
  
### Returns:  
- 0: dependencies were found  
- 1: failed to find dependencies  
  
  
  
## DEVKIT\_PROJECT\_INIT()  
  
  
  
Initialize a new project  
  
### Parameters:  
- $1: template directory  
- $2: projects directory  
- $3: project name  
  
### Expects:  
- MODULEDIR  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_RUN\_CONTAINER()  
  
  
  
Run Docker container  
  
### Parameters:  
- $1: container image  
- $2: container name  
- $3: projects directory  
- $4: web server port  
- $5: RPC port  
- $6: WS port  
- $7: remixd port  
- $8: CORS policy (optional)  
- $9: genesis configuration file (optional)  
- $10: path to nginx directory (optional)  
  
### Returns:  
- Non-zero on failure.  
  
  
  
## DEVKIT\_STOP\_CONTAINER()  
  
  
  
Stop Docker container  
  
### Parameters:  
- $1: container name  
  
### Returns:  
- Non-zero on failure.  
  
  
  
## DEVKIT\_CONSOLE()  
  
  
  
Open console to geth  
  
### Parameters:  
- $1: Geth IPC address  
  
### Expects:  
- GETH to point to go-ethereum, defaults to "geth".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_ACCOUNT\_TRANSFER()  
  
  
  
Transfer funds from one account to another.  
  
### Parameters:  
- $1: ipc address  
- $2: From address  
- $3: To address  
- $4: value in wei  
  
### Expects:  
- GETH to point to go-ethereum, defaults to "geth".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_ACCOUNT\_LIST()  
  
  
  
List available accounts.  
  
### Parameters:  
- $1: ipc address  
  
### Expects:  
- GETH to point to go-ethereum, defaults to "geth".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_ACCOUNT\_SIGN()  
  
  
  
Sign a transaction  
  
### Parameters:  
- $1: network host address  
- $2: Chain ID  
- $3: from address  
- $4: to address  
- $5: transaction data  
- $6: transaction value  
- $7: account private key  
- $8: gas price  
- $9: gas limit  
  
### Expects:  
- NODE to point to node.js, defaults to "node".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_ACCOUNT\_SEED()  
  
  
  
Generate a random 12-word seed  
  
### Parameters:  
- None  
  
### Expects:  
- NODE to point to node.js, defaults to "node".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_COMPILE\_PROJECT()  
  
  
  
Compile a smart contract.  
  
### Parameters:  
- $1: projects home directory  
- $2: project name  
- $3: optimization flag  
- $4: recompile flag  
- $5: contract name  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## \_DEVKIT\_COMPILE\_ETHEREUM()  
  
  
  
Compile for Ethereum  
  
### Parameters:  
- $1: source file path  
- $2: contract name  
- $3: optimization flag  
- $4: recompile flag  
  
### Expects:  
- SOLC to point to solidity compiler, defaults to "solc".  
  
### Returns:  
- Non-zero on error  
  
  
  
## DEVKIT\_INFO\_PROJECT()  
  
  
  
Get info on smart contracts for the project.  
  
### Parameters:  
- $1: Geth IPC address  
- $2: projects home directory  
- $3: project name  
- $4: blockchain environment name  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_BUILD\_PROJECT()  
  
  
  
Build a smart contract to the network.  
  
### Parameters:  
- $1: Geth IPC address  
- $2: projects home directory  
- $3: project name  
- $4: blockchain environment name  
- $5: rebuild flag  
- $6: wallets for signing  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## \_DEVKIT\_DAPPFILE\_JS()  
  
  
  
Get the app confuration for js destination file.  
  
### Parameters:  
- $1: Filepath of Dappfile.yaml  
  
### Returns:  
- Line on stdout.  
  
  
  
## \_DEVKIT\_DAPPFILE\_CONTRACT\_POSTDEPLOY()  
  
  
  
List all post deploy entries for a contract and its associated data line by line.  
  
### Parameters:  
- $1: Filepath of Dappfile.yaml  
- $2: the name of the environment we are working on  
- $3: the index of the contract to get post deployments for  
  
### Returns:  
- Lines on stdout.  
  
  
  
## \_DEVKIT\_DAPPFILE\_CONTRACTS()  
  
  
  
List all contracts and their associated data line by line.  
  
### Parameters:  
- $1: Filepath of Dappfile.yaml  
- $2: the name of the environment we are working on (optional).  
- $3: the index of the contract to get, -1 means all.  
  
### Returns:  
- Lines on stdout.  
  
  
  
## \_DEVKIT\_DAPPFILE\_CONTRACT\_POSTDEPLOY\_ARGS()  
  
  
  
List all arguments for a contracts post deploys  
  
### Parameters:  
- $1: Filepath of Dappfile.yaml  
- $2: Contract index  
- $3: Post Deploy index  
  
### Returns:  
- Lines on stdout.  
  
  
  
## \_DEVKIT\_DAPPFILE\_CONTRACT\_ARGS()  
  
  
  
List all arguments for a contract  
  
### Parameters:  
- $1: Filepath of Dappfile.yaml  
- $2: Contract index  
  
### Returns:  
- Lines on stdout.  
  
  
  
## \_DEVKIT\_BUILD()  
  
  
  
Inner implementation of build  
  
### Parameters:  
- $1: projects home directory  
- $2: deployment directory  
- $3: backup directory  
- $4: blockchain name  
- $5: network name  
- $6: source file  
- $7: contract file  
- $8: account  
- $9: address  
- $10: key for signing  
- $11: gas limit  
- $12: IPC address  
- $13: rebuild flag  
- $14: Contract index  
- $15: arguments  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_BUILD\_ETHEREUM\_EXTERNAL()  
  
  
  
Deploy to external Ethereum geth node via HTTP  
  
### Parameters:  
- $1: projects home directory  
- $2: deployment directory  
- $3: backup directory  
- $4: source file  
- $5: contract file  
- $6: account  
- $7: address  
- $8: key for signing  
- $9: IPC address  
- $10: network  
- $11: external host address  
- $12: external network ID  
- $13: gas limit  
- $14: gas price  
- $15: rebuild flag  
- $16: Contract index  
- $17: arguments  
  
### Expects:  
- GETH  
- \_deploy\_pending: array  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_ETHEREUM\_COUNT\_FN\_ARGS()  
  
  
  
Count function arguments  
  
### Parameters:  
- $1: source file  
- $2: contract file  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_ETHEREUM\_ADD\_ARGS()  
  
  
  
Add arguments  
  
### Parameters:  
- $1: IPC address  
- $2: ABI file  
- $3: bin file  
- $4: arguments  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_BUILD\_ETHEREUM\_CB()  
  
  
  
Callback function to check if a contract has been deployed.  
  
### Parameters:  
- $1: line  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_ETHEREUM\_GET\_CONTRACT()  
  
DEVKIT\_ETHEREUM\_GET\_CONTRACT  
  
Return the contract binary prefixed by 0x,  
or empty string if not found.  
  
### Parameters:  
- $1: host address  
- $2: contract address  
  
### Expects:  
- NODE to point to node.js, defaults to "node".  
  
### Returns:  
- Contract bytecode. Non-zero on error  
  
  
  
## \_DEVKIT\_POST\_DEPLOY()  
  
  
  
Process post deployment  
  
### Parameters:  
- $1: IPC address  
- $2: projects home directory  
- $3: project name  
- $4: environment name  
  
### Expects:  
- $\_deploy\_done  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_BUILD\_WAIT()  
  
  
  
Wait for build step to complete  
  
### Parameters:  
- None  
  
### Expects:  
- $\_deploy\_pending  
- $\_deploy\_done  
  
### Returns:  
- Non-zero on error  
  
  
  
## DEVKIT\_JS\_BUILD\_PROJECT()  
  
  
  
### Parameters:  
- $1: projects home directory  
- $2: project name  
- $3: environment name  
- $4: wallets list  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_TEST()  
  
  
  
Test a contract  
  
### Parameters:  
- $1: projects home directory  
- $2: project name  
- $3: contract file name  
- $4: specific test name (optional)  
- $5: hot reload flag (optional)  
- $6: break on first failure flag (optional)  
  
### Expects:  
- TESTER path to tester binary  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_GET\_RECEIPT()  
  
  
  
Read a transaction receipt  
  
### Parameters:  
- $1: Geth node IPC endpoint  
- $2: receipt hash  
  
### Expects:  
- GETH to point to go-ethereum, defaults to "geth".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_CONVERT\_TO\_ETHER()  
  
  
  
Convert to ether  
  
### Parameters:  
- $1: value  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_CONVERT\_TO\_WEI()  
  
  
  
Convert to wei  
  
### Parameters:  
- $1: value  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_ADD\_ARGS()  
  
  
  
Append arguments to existing compiled binary file  
This operation considers that arguments are always  
appended the same way and not packed differently in  
optimized builds.  
  
### Parameters:  
- $1: IPC address  
- $2: projects home directory  
- $3: project name  
- $4: contract file name  
- $@: arguments  
  
### Expects:  
- SOLC  
- GETH  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## \_DEVKIT\_QUOTE\_ARG()  
  
  
  
Helper function for escaping and quoting arguments in place  
  
### Parameters:  
- $1: variable to quote  
  
### Returns:  
- Non-zero on failure  
  
  
  
## \_DEVKIT\_PARSE\_DAPPFILE()  
  
DEVKIT\_PARSE\_DAPPFILE  
  
Parse Dapp configuration file  
  
### Parameters:  
- None  
  
### Returns:  
- Non-zero on failure  
  
  
  
## DEVKIT\_MAKE\_PROJECT()  
  
  
  
Perform all steps needed to completely make a project  
  
### Parameters:  
- $1: projects home directory  
- $2: project name  
- $3: optimization flag  
- $4: recompile flag  
- $5: IPC address  
- $6: environment name  
- $7: rebuild flag  
- $8: wallets  
  
### Returns:  
- Non-zero on failure  
  
  
  
## \_DEVKIT\_ENV\_WRAP()  
  
  
  
When being run inside a container as root,  
we will try to run as a user with the same uid  
as the owner of the project on the host disk.  
  
### Parameters:  
- None  
  
### Returns:  
- Non-zero on error  
  
  
  
## DEVKIT\_WATCH\_EVENTS()  
  
  
  
Watch all events from one or more contracts  
  
### Parameters:  
- $1: ipc address  
- $2: ABI file  
- $3: contract address  
- $4: event name  
- $5: from block number  
- $6: to block number  
  
### Expects:  
- GETH to point to go-ethereum, defaults to "geth".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## \_WEB3\_JSON\_EXTRACT()  
  
WEB3  
  
Handles JSON data reading  
  
### Parameters:  
- $1: json file  
- $2: path  
  
### Returns:  
- Non-zero on failure  
  
  
  
## \_DEVKIT\_DAPPFILE\_ACCOUNT()  
  
DEVKIT\_DAPPFILE\_ACCOUNT  
  
Retrieve account information contained in a given wallet  
  
### Parameters:  
- $1: Dappfile path  
- $2: environment name  
- $3: account address  
- $4: blockchain name  
- $5: wallets list  
  
### Expects:  
- NODE to point to node.js, defaults to "node".  
  
### Returns:  
- out\_address  
- out\_privkey  
- Non-zero on error  
  
  
  
