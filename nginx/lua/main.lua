local cjson = require 'cjson'
local reqargs = require 'reqargs'
local ws_server = require "resty.websocket.server"
local posix = require "posix"
local pwait = require "posix.sys.wait"
local fcntl = require "posix.fcntl"
local unistd = require "posix.unistd"
local posix_stat = require "posix.sys.stat"
local lyaml = require("lyaml")


local Routes = {GET={},POST={}}
local _M = {}

function Wrapper()
    local _files = {}

    local function send(id, data)
        -- Add data to buffer.
        _files[id].buffer = _files[id].buffer .. data
    end

    local function _close(id)
        if _files[id].isPipe then
           local pfd = _files[id].isPipe
           unistd.close(pfd.fd)
           --for i = 1, #pfd.pids - 1 do
               --pwait.wait(pfd.pids[i])
           --end
           --ngx.log(ngx.ERR, "PID", pfd.pids[1])
           --local pid, msg, status = pwait.wait(pfd.pids[#pfd.pids])

            local pid, msg, status = posix.pclose(_files[id].isPipe)
            --ngx.log(ngx.ERR, "return", pid, msg, status)
            _files[id].status = _files[id].status or (type(status) == "number" and status or 0)
            _files[id].fd = nil
        else
            unistd.close(_files[id].fd)
            _files[id].status = _files[id].status or 0
            _files[id].fd = nil
        end
    end

    local function invalidate()
        for id, File in pairs(_files) do
            if File.fd then
                _close(id)
            end
        end
    end

    local function write(id)
        -- Write from buffer all that we can out on file.
        local fd = _files[id].fd
        local s = _files[id].buffer
        while true do
            local success, count = pcall(unistd.write, fd, s)
            if not success then
                invalidate()
                return
            end
            if count == nil or count == 0 then
                break
            end
            s = s:sub(count + 1)
            if #s == 0 then
                break
            end
        end
        _files[id].buffer = s
    end

    local function read(id, size)
        -- Read from file into buffer.
        size = size or 1024
        local fd = _files[id].fd
        local s = ""
        while true do
            local data = unistd.read(fd, size > 0 and size or 1024)
            if data == nil or #data == 0 then
                break
            end
            s = s .. data
            if size > 0 and #s >= size then
                break
            end
        end
        _files[id].buffer = _files[id].buffer .. s
    end

    local function readLine(id)
         --Read a line in blocking mode and return it.
         --Bypasses the buffer.
        local f = fcntl.fcntl(_files[id].fd, fcntl.F_SETFL, fcntl.O_BLOCK)
        if not f then
            --log("Error: could not BLOCK fd.")
            os.exit(99)
        end
        local line = ""
        while true do
            local byte = unistd.read(_files[id].fd, 1)
            if byte == "\n" then
                break
            end
            line = line .. byte
        end
        local f = fcntl.fcntl(_files[id].fd, fcntl.F_SETFL, fcntl.O_NONBLOCK)
        if not f then
            --log("Error: could not NONBLOCK fd.")
            os.exit(99)
        end
        return line
    end

    local function recv(id)
        -- Return buffered data and clear it.
        local data = _files[id].buffer
        _files[id].buffer = ""
        return data
    end

    local function isEmpty(id)
        return #_files[id].buffer == 0
    end

    local function getPoll(fds)
        local count = 0
        for id, File in pairs(_files) do
            if File.fd then
                File.events = {}
                File.revents = nil
                if File.mode == "r" then
                    if #File.buffer < 1024*10 then
                        File.events.IN = true
                    end
                elseif File.mode == "w" then
                    if #File.buffer > 0 then
                        File.events.OUT = true
                    end
                end
                count = count + 1
                fds[File.fd] = File
            end
        end
        return count
    end

    local function isValid()
        -- Returns true if any file is still open.
        for id, File in pairs(_files) do
            if File.fd then return true end
        end
        return false
    end

    local function status(id)
        return _files[id].status
    end

    local function setStatus(id, code)
        if not _files[id].status then
            _files[id].status=code
        end
    end

    local function postPoll()
        local hadHup = false
        for id, File in pairs(_files) do
            if File.fd then
                if File.revents.IN then
                    read(id, 0)
                elseif File.revents.OUT then
                    write(id)
                end
                if File.revents.HUP then
                    _close(id)
                    hadHup = true
                elseif File.revents.ERR then
                    _close(id)
                    hadHup = true
                end
            end
        end
        if hadHup then
            invalidate()
        end
    end

    local function newFile(id, fd, mode, isPipe)
        _files[id] = {
            id = id,
            fd = fd,
            buffer = "",
            mode = mode,
            isPipe = isPipe,
        }
        local f = fcntl.fcntl(_files[id].fd, fcntl.F_SETFL, fcntl.O_NONBLOCK)
        if f == -1 then
            --log("Error: could not NONBLOCK fd.")
            os.exit(99)
        end
    end

    return {
        send = send,
        write = write,
        read = read,
        readLine = readLine,
        recv = recv,
        isEmpty = isEmpty,
        getPoll = getPoll,
        postPoll = postPoll,
        newFile = newFile,
        isValid = isValid,
        invalidate = invalidate,
        status = status,
        setStatus = setStatus,
    }
end

function _M._response(obj, status)
    status = status or 200
    ngx.header.content_type = "application/json"
    local body = cjson.encode(obj or {})
    ngx.status = status
    ngx.print(string.format("%s\n", body))
end

function _M._route()
    local params={}
    params.get, params.post, params.files = reqargs({})
    params.header = ngx.req.get_headers()
    params.uri = ngx.var.uri
    params.location = ngx.var.location
    params.path = params.uri:sub(#params.location)
    params.request_uri = ngx.var.request_uri
    params.method = ngx.req.get_method():upper()
    --_M._response(params)
    --if 1 then return end

    local routes = Routes[params.method] or {}
    local fn
    local matches
    for _,o in ipairs(routes) do
        local routePath, route = o[1], o[2]
        matches = {params.path:match(routePath)}
        if matches[1] ~= nil then
            fn = route
            break
        end
    end

    if not fn then
        _M._response(nil, 404)
    else
        fn(matches, params)
    end
end

-- List all projects, return the directory names.
function _M.getProjects()
    -- List all directories in projects home dir which have a Dappfile.yaml inside of them.
    local cmd="sh -c \"find /home/devkit/projects/ -type f -name Dappfile.yaml -maxdepth 2 | cut -d/ -f5\""
    local fd = io.popen(cmd)
    if not fd then
        _M._response(nil, 500)
        return
    end
    local lines = fd:read("*a")
    fd:close()
    local response={}
    for dir in lines:gmatch("[^\n]*") do
        if #dir > 0 then
            local file=string.format("%s/%s/Dappfile.yaml", "/home/devkit/projects", dir)
            local fd = io.open(file)
            local contents = fd:read("*a")
            fd:close()
            local success, dappfile = pcall(lyaml.load, contents)
            if success and dappfile then
                response.projects = response.projects or {}
                local stat = posix_stat.stat(file)
                local inode=stat["st_ino"]
                local hash=_M._hashFile(file)
                table.insert(response.projects, {dappfile=dappfile,dappfile_hash=hash,type="dapp",dir=dir,inode=inode})
            else
                --ngx.log(ngx.ERR, string.format("Invalid Dappfile.yaml in %s.", file))
            end
        end
    end
    _M._response(response)
end

function _M._hashFile(file)
    local cmd=string.format("sh -c \"sha1sum %s | awk '{print \\$1}'\"", file)
    local fd = io.popen(cmd)
    if not fd then
        return "no_hash"
    end
    local hash = fd:read()
    fd:close()
    return hash
end

function _M.saveProject(matches, params)
    local dir=matches[1]
    local hash=params.post.hash;
    local file=string.format("%s/%s/Dappfile.yaml", "/home/devkit/projects", dir)
    local hash2=_M._hashFile(file)
    if hash2==nil then
        _M._response({status=1,msg="Could not access project."})
        return
    end
    if hash~=hash2 then
        _M._response({status=2,msg="hashes do not match."})
        return
    end
    for k,v in ipairs(params.post.dappfile.contracts or {}) do
        if v.args and #v.args == 0 then
            v.args=nil
        end
    end
    local success, content = _M.yamlDump(params.post.dappfile)
    if not success then
        _M._response({status=3,msg="Could not parse dappfile data."})
        return
    end
    local fd = io.open(file, "w")
    fd:write(content)
    fd:close()
    local newhash=_M._hashFile(file)
    _M._response({status=0,msg="Saved.",hash=newhash})
end

function _M.proxyUpdate(matches, params)
    local executable = "/home/devkit/tools/nginx-update-conf.sh"
    io.popen(executable)
    _M._response({status=0,msg="Ok"})
end

function _M.listFiles(matches, params)
    local dir=matches[1]
    local path=params.post.path;
    -- TODO: sec check on path and dir
    local files={{}}
    local ftype="d"
    while true do
        local path=string.format("%s/%s%s", "/home/devkit/projects", dir, path)
        local fd = io.popen(string.format("cd %s && find ./ -type %s -maxdepth 1 | sed \"s|^\\./||\"", path, ftype))
        if not fd then
            _M._response({status=1,msg="Error"})
            return
        end
        for file in fd:lines() do
            if #file>0 and file:sub(1,1) ~= '.' then
                table.insert(files, {name=file,type=ftype})
            end
        end
        fd:close()
        if ftype=="f" then break end
        ftype="f"
    end
    _M._response({status=0,msg="Listing",files=files})
end

function _M.newFile(matches, params)
    local dir=matches[1]
    local path=params.post.path;
    local file=params.post.file;
    if path:match("%.%.") or dir:match("%.%.") or path:sub(-1) ~= "/" or path:sub(1,1) ~= "/" then
        _M._response({status=1,msg="Bad path"})
        return
    end
    if not file:match("^([^/]+)/?$") then
        _M._response({status=1,msg="Bad path"})
        return
    end
    local newfile
    if file:sub(-1) == "/" then
        newfile=string.format("%s%s%s%s", "/home/devkit/projects/", dir, path, file)
        ngx.log(ngx.ERR, newfile)
        io.popen(string.format("/bin/mkdir %s", newfile))
    else
        newfile=string.format("%s%s%s%s", "/home/devkit/projects/", dir, path, file)
        ngx.log(ngx.ERR, newfile)
        io.popen(string.format("/bin/touch %s", newfile))
    end
    local stat = posix_stat.stat("/home/devkit/projects")
    posix.chown(newfile, stat.st_uid, stat.st_uid)
    _M._response({status=0,msg="Ok"})
end

function _M.deleteFile(matches, params)
    local dir=matches[1]
    local path=params.post.path;
    if path:match("%.%.") or dir:match("%.%.") or path:sub(1,1) ~= "/" then
        _M._response({status=1,msg="Bad path"})
        return
    end
    local file=string.format("%s%s%s", "/home/devkit/projects/", dir, path)
    io.popen(string.format("/bin/rm -rf %s", file))
    _M._response({status=0,msg="Ok"})
end

function _M.renameFile(matches, params)
    local dir=matches[1]
    local path=params.post.path;
    local file=params.post.file;
    if path:match("%.%.") or dir:match("%.%.") or path:sub(1,1) ~= "/" then
        _M._response({status=1,msg="Bad path"})
        return
    end
    if not file:match("^[^/]+$") then
        _M._response({status=1,msg="Bad path"})
        return
    end
    if path:sub(-1) == "/" then path=path:sub(1,-2) end
    local srcdir=path:match("^(.*)/[^/]+$") or "."
    local src=string.format("%s%s%s", "/home/devkit/projects/", dir, path)
    local dest=string.format("%s%s%s/%s", "/home/devkit/projects/", dir, srcdir, file)
    io.popen(string.format("/bin/mv %s %s", src, dest))
    _M._response({status=0,msg="Ok"})
end

function _M.loadFile(matches, params)
    local dir=matches[1]
    local path=params.post.path;
    -- TODO: sec check on path and dir
    local file=string.format("%s/%s/%s", "/home/devkit/projects", dir, path)
    local fd = io.open(file)
    if not fd then
        _M._response({status=1,msg="New file",contents="",hash="deadbeef"})
        return
    end
    local contents = fd:read("*a")
    fd:close()
    local hash=_M._hashFile(file)
    _M._response({status=0,msg="File found",contents=contents,hash=hash})
end

function _M.saveFile(matches, params)
    posix.umask("744")
    local dir=matches[1]
    local path=params.post.path;
    local hash=params.post.hash;
    local contents=params.post.contents
    -- TODO: sec check on path and dir
    local file=string.format("%s/%s/%s", "/home/devkit/projects", dir, path)
    if hash=="deadbeef" then
        -- check that file yet doesn't exist.
        local fd = io.open(file, "r")
        if fd then
            fd:close()
            _M._response({status=2,msg="hashes do not match."})
            return
        end
    else
        local hash2=_M._hashFile(file)
        if hash2==nil then
            _M._response({status=1,msg="Could not access project."})
            return
        end
        if hash~=hash2 then
            _M._response({status=2,msg="hashes do not match."})
            return
        end
    end
    -- Get user ID
    local stat = posix_stat.stat("/home/devkit/projects")
    local fd = io.open(file, "w")
    fd:write(contents)
    fd:close()
    posix.chown(file, stat.st_uid, stat.st_uid)
    local newhash=_M._hashFile(file)
    _M._response({status=0,msg="Saved.",hash=newhash})
end

function _M.yamlDump(obj)
    function stripFn(obj)
        local itsa=type(obj)
        if itsa == 'table' then
            if #obj >0 then
                for k,v in ipairs(obj) do
                    if type(v) == 'function' then
                        obj[k] = nil
                    elseif type(v) == 'table' then
                        stripFn(v)
                    end
                end
            else
                for k,v in pairs(obj) do
                    if type(v) == 'function' then
                        obj[k] = nil
                    elseif type(v) == 'table' then
                        stripFn(v)
                    end
                end
            end
        end
    end
    stripFn(obj)
    return pcall(lyaml.dump, {obj})
end

function _M.fundAccount(matches, params)
    local account=matches[1]
    local from="0x8833af1a9b1e7ddc837ed2c771078dcf3ac23daf"
    local value="1000000000000000000"
    local executable = "./wrapper.sh"
    local args_space = string.format('space -m gitlab.com/web3-sh/devkit /account/transfer/ -e container= -e from=%s -e value=%s -e to=%s', from, value, account)
    local args={args_space}
    _M.execute(executable, args, true)
end

function _M.initProject(matches, params)
    --local args={"-c", string.format('SPACE_LOG_ENABLE_COLORS=0 HOME=/root /usr/local/bin/space -m gitlab.com/web3-sh/devkit /init/ -e projectshome=/home/devkit/projects -e project=%s -e template=%s,project, template')}
    local project=matches[1]
    local template=params.get.template or ""
    local title=params.get.title or ""
    local executable = "./wrapper.sh"
    local args_space = string.format('space -m gitlab.com/web3-sh/devkit /init/ -e projectshome=/home/devkit/projects -e project=%s -e template=%s -e title=%s', project, template, title)
    local args={args_space}
    _M.execute(executable, args, true)
end

function _M.makeProject(matches, params)
    local project=matches[1]
    local wallets=params.get.wallets or ""
    local executable = "./wrapper.sh"
    local args_space = string.format('space -m gitlab.com/web3-sh/devkit /make/ -e container= -e projectshome=/home/devkit/projects -e project=%s -e wallets=%s', project, wallets)
    local args={args_space}
    _M.execute(executable, args, true)
end

function _M.compileProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "./wrapper.sh"
    local args_space = string.format('space -m gitlab.com/web3-sh/devkit /compile/ -e container= -e projectshome=/home/devkit/projects -e project=%s -- %s', project, posargs)
    local args={args_space}
    _M.execute(executable, args, true)
end

function _M.compileContract(matches, params)
    local project=matches[1]
    local contract=matches[2]
    local executable = "./wrapper.sh"
    local args_space = string.format('space -m gitlab.com/web3-sh/devkit /compile/ -e container= -e projectshome=/home/devkit/projects -e project=%s -e contract=%s -e rebuild=%s -e optimization=%s', project, contract, "true", "true")
    ngx.log(ngx.ERR, args_space)
    local args={args_space}
    _M.execute(executable, args, true)
end

function _M.testProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "./wrapper.sh"
    local args_space = string.format('space -m gitlab.com/web3-sh/devkit /test/ -e container= -e projectshome=/home/devkit/projects -e project=%s', project)
    local args={args_space}
    _M.execute(executable, args, true)
end

function _M.buildProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "./wrapper.sh"
    local args_space = string.format('space -m gitlab.com/web3-sh/devkit /build/ -e container= -e projectshome=/home/devkit/projects -e project=%s -e rebuild=true -- %s', project, posargs)
    local args={args_space}
    _M.execute(executable, args, true)
end

function _M.infoProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "./wrapper.sh"
    local args_space = string.format('space -m gitlab.com/web3-sh/devkit /info/ -e container= -e projectshome=/home/devkit/projects -e project=%s -- %s', project, posargs)
    local args={args_space}
    _M.execute(executable, args, true)
end

function _M.eventsProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "./wrapper.sh"
    local args_space = string.format('space -m gitlab.com/web3-sh/devkit /events/ -e container= -e projectshome=/home/devkit/projects -e project=%s -- %s', project, posargs)
    local args={args_space}
    _M.execute(executable, args, true)
end

function _M.buildJSProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "./wrapper.sh"
    local args_space = string.format('space -m gitlab.com/web3-sh/devkit /js/ -e container= -e projectshome=/home/devkit/projects -e project=%s -- %s', project, posargs)
    local args={args_space}
    _M.execute(executable, args, true)
end

function _M.webMakeProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "./wrapper.sh"
    local args_space = string.format('( cd /home/devkit/projects/%s && /usr/local/bin/space /app/make/ -e container= -- %s )', project, posargs)
    local args={args_space}
    _M.execute(executable, args, true)
end

function _M.webTestProject(matches, params)
    local executable = "./wrapper.sh"
    local args={"aaffe"}
    _M.execute(executable, args, true)
end

function _M.execute(executable, args, isWrapped)
    local proxy = Wrapper()

    local stdoutfdr,stdoutfdw = unistd.pipe()
    proxy.newFile(1, stdoutfdr, "r")

    local stderrfdr,stderrfdw = unistd.pipe()
    proxy.newFile(2, stderrfdr, "r")

    local stdinfdr,stdinfdw = unistd.pipe()
    local fd1 = posix.dup(1)
    local fd2 = posix.dup(2)
    posix.dup2(stdoutfdw, 1)
    posix.dup2(stderrfdw, 2)
    local pfd = posix.popen({executable, args}, "w", function() return stdinfdr, stdinfdw end)
    proxy.newFile(0, stdinfdw, "w", pfd)
    posix.dup2(fd1, 1)
    posix.dup2(fd2, 2)
    posix.close(fd1)
    posix.close(fd2)

    if not pfd then
        ngx.log(ngx.ERR, "Error popen for process.")
        ngx.exit(403)
        return
    end

    function ws()
        -- https://github.com/openresty/lua-resty-websocket
        local wb, err = ws_server:new{
            timeout = 5000,  -- in milliseconds
            max_payload_len = 65535,
        }
        if not wb then
            ngx.log(ngx.ERR, "ws: failed to create new socket: ", err)
            return false
        end

        while true do
            local fds = {}
            local c2 = proxy.getPoll(fds)
            if c2 == 0 then
                break
            end

            posix.poll(fds, -1)
            proxy.postPoll()

            -- Move data from proxy to ws.
            local toSend=""
            local data = proxy.recv(1)
            if isWrapped then
                -- Check for exit code wrapped in stream
                local i1,i2,ret = data:find('%---exitcode:(%d+)\n$')
                if i1 then
                    data=data:sub(1,i1-1)
                    proxy.setStatus(0, ret)
                end
            end
            if #data > 0 then
                toSend = string.format("%s1\n%d\n%s", toSend, #data, data)
            end
            local data = proxy.recv(2)
            if #data > 0 then
                toSend = string.format("%s2\n%d\n%s", toSend, #data, data)
            end
            if not proxy.isValid() then
                -- The proxy has ended, send the status code on ws.
                toSend = string.format("%s0\n%d\n", toSend, proxy.status(0))
            end

            while #toSend > 0 do
                local bytes, err = wb:send_text(toSend)
                if not bytes then
                    ngx.log(ngx.ERR, "ws: failed to send a text frame: ", err)
                    return false
                end
                break
            end
            if not proxy.isValid() then
                break
            end
        end

        local bytes, err = wb:send_close(1000)
        if not bytes then
            ngx.log(ngx.ERR, "ws: failed to send the close frame: ", err)
            return false
        end

        return true
    end

    if not ws() then
        ngx.exit(444)
        return
    end

    ngx.exit(200)
end

table.insert(Routes.GET, {"^/(projects)/$", _M.getProjects})
table.insert(Routes.POST, {"^/proxy/update/$", _M.proxyUpdate})
table.insert(Routes.POST, {"^/project/([a-zA-Z0-9_%-.]+)/ls/$", _M.listFiles})
table.insert(Routes.POST, {"^/project/([a-zA-Z0-9_%-.]+)/file/delete/$", _M.deleteFile})
table.insert(Routes.POST, {"^/project/([a-zA-Z0-9_%-.]+)/file/rename/$", _M.renameFile})
table.insert(Routes.POST, {"^/project/([a-zA-Z0-9_%-.]+)/file/new/$", _M.newFile})
table.insert(Routes.POST, {"^/project/([a-zA-Z0-9_%-.]+)/file/load/$", _M.loadFile})
table.insert(Routes.POST, {"^/project/([a-zA-Z0-9_%-.]+)/file/save/$", _M.saveFile})
table.insert(Routes.POST, {"^/project/([a-zA-Z0-9_%-.]+)/save/$", _M.saveProject})
table.insert(Routes.GET, {"^/project/([a-zA-Z0-9_%-.]+)/init/$", _M.initProject})
table.insert(Routes.GET, {"^/project/([a-zA-Z0-9_%-.]+)/make/$", _M.makeProject})
table.insert(Routes.GET, {"^/project/([a-zA-Z0-9_%-.]+)/compile/$", _M.compileProject})
table.insert(Routes.GET, {"^/project/([a-zA-Z0-9_%-.]+)/contract/(.+)/compile/$", _M.compileContract})
table.insert(Routes.GET, {"^/project/([a-zA-Z0-9_%-.]+)/build/$", _M.buildProject})
table.insert(Routes.GET, {"^/project/([a-zA-Z0-9_%-.]+)/info/$", _M.infoProject})
table.insert(Routes.GET, {"^/project/([a-zA-Z0-9_%-.]+)/events/$", _M.eventsProject})
table.insert(Routes.GET, {"^/project/([a-zA-Z0-9_%-.]+)/js/$", _M.buildJSProject})
table.insert(Routes.GET, {"^/project/([a-zA-Z0-9_%-.]+)/test/$", _M.testProject})
table.insert(Routes.GET, {"^/project/([a-zA-Z0-9_%-.]+)/app_make/$", _M.webMakeProject})
table.insert(Routes.GET, {"^/account/(.+)/fund/$", _M.fundAccount})

_M._route()
