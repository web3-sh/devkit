#!/usr/bin/env sh

if [ "$(id -u)" = 0 ]; then
    if ! uid=$(stat -c %u /home/devkit/projects); then
        print "%s\\n" "Internal error in wrapper.sh, could not stat /home/devkit/projects." >&2
        printf "---exitcode:%d\\n" "255"
        exit 255
    fi
    if [ ! -d /home/space ]; then
        print "%s\\n" "Internal error in wrapper.sh, /home/space non existent." >&2
        printf "---exitcode:%d\\n" "255"
        exit 255
    fi
    if ! id -u "user${uid}" >/dev/null 2>/dev/null; then
        if ! adduser -u "${uid}" -D "user${uid}"; then
            printf "%s\\n" "Internal error in wrapper.sh, could not adduser." >&2
            printf "---exitcode:%d\\n" "255"
            exit 255
        fi
        addgroup "user${uid}" space
    fi

    path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
    bash=$(command -v bash)
    su -l "user${uid}" -s "${bash}" -c "export PATH=\"${path}\" NODE_PATH=/home/devkit/node_modules SPACE_LOG_ENABLE_COLORS=0 SPACE_MODULES_SHARED=/home/space; $@"
else
    bash -c "$@"
fi

printf "%s%d\\n" "---exitcode:" "$?"
