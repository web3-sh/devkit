#!/usr/bin/env bash

fixConf()
{
    local dir="${1}"
    shift

    local original_data=
    original_data=$(cat "${dir}/nginx.conf.original") || return 1

    local proxy_data=
    proxy_data=$(getProxies) || return 1

    local new_data="${original_data/\#\#\# PROXIES HERE PL0X \#\#\#/${proxy_data}}"

    writeConf "${dir}/nginx.conf.test" "${new_data}" || return 1

    testConf "${dir}/nginx.conf.test" || return 1

    writeConf "${dir}/nginx.conf" "${new_data}" || return 1

    reloadNginx
}

getProxies()
{
    local data=""
    local project=
    while read -r project; do
        local dappfile_path="/home/devkit/projects/${project}/Dappfile.yaml"
        local content=
        content=$(cat "${dappfile_path}")
        local port=
        port=$(printf "%s\\n" "${content}" | luajit -e '
        lyaml=require"lyaml";
        local t=lyaml.load(io.read("*all"));

        function str(s, default)
            if type(s) == "table" then
                return default
            end
            return s or default
        end

        if t then
            print(str(((((t.config or {}).app or {}).serve or {}).proxy or {}).port or ""))
        end
')
    if [ -n "${port}" ]; then
        data="${data}
        server {
            listen 80;
            server_name ${project};
            location / {
                proxy_pass http://127.0.0.1:${port};
            }
        }
"
    fi
    done < <(find /home/devkit/projects/ -type f -name Dappfile.yaml -maxdepth 4 | cut -d/ -f5)

    printf "%s\\n" "${data}"
}

writeConf()
{
    local file="${1}"
    shift
    local data="${1}"
    printf "%s\\n" "${data}" > "${file}"
}

testConf()
{
    local file="${1}"
    shift

    /usr/local/openresty/bin/openresty -t -c ${file}
}

reloadNginx()
{
    local pid=
    pid=$(pgrep nginx | head -n 1)
    kill -s hup "${pid}"
}

if [ "$(id -u)" = 0 ]; then
    fixConf "/home/devkit/nginx"
else
    printf "%s\\n" "[ERROR] This must be run as root." >&2
    ( exit 1 )
fi

printf "%s%d\\n" "---exitcode:" "$?"
