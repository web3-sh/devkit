//
// Copyright 2017 Blockie AB
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation version 3 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

// ===================
// Dependencies
// ===================
assert = require('assert');
fs = require('fs');
web3 = require('web3');

// ===================
// Connection
// ===================
console.log("Connecting to: " + node_address);
web = new web3(new web3.providers.HttpProvider(node_address));

// ===================
// Contract
// ===================
var contract_abi_file= "./" + contract_name + ".abi";
var contract_bin_file= "./" + contract_name + ".bin";
if (fs.existsSync(contract_abi_file) && fs.existsSync(contract_bin_file)) {
    console.log("Using existing files: " + contract_abi_file + " " + contract_bin_file);
    contract_interface = JSON.parse(fs.readFileSync(contract_abi_file).toString());
    test_contract = web.eth.contract(contract_interface);
    contract_bytecode = fs.readFileSync(contract_bin_file).toString();
}
else {
    console.log("Binary files are missing. Contract " + contract_name + " must be compiled before testing.");
    process.exit(1);
}

// ===================
// Accounts
// ===================
address_zeus = web.eth.accounts[0];
console.log("\n  Zeus account: " + address_zeus);

// ===================
// Settings
// ===================
console.log("  Node address: " + node_address);
console.log("  Contract name: " + contract_name);
console.log("  Contract file path: " + contract_file_path);

// Operations
contract_instance=0;

function contract_setup(done) {
    // Setup arguments
    // TODO: FIXME: there should be a better way to pack arguments
    if (contract_params.length == 0) {
        contract_bytecode = test_contract.new.getData({data: contract_bytecode});
    }
    else if (contract_params.length == 1) {
        contract_bytecode = test_contract.new.getData(contract_params[0], {data: contract_bytecode});
    }
    else if (contract_params.length == 2) {
        contract_bytecode = test_contract.new.getData(contract_params[0], contract_params[1], {data: contract_bytecode});
    }
    else if (contract_params.length == 3) {
        contract_bytecode = test_contract.new.getData(contract_params[0], contract_params[1], contract_params[2], {data: contract_bytecode});
    }
    else {
        console.error("Error setting up constructor parameters");
        return;
    }

    gasEstimate = web.eth.estimateGas({from: address_zeus, data: '0x'+contract_bytecode, gasPrice: 1000000000});

    web.eth.sendTransaction({from: address_zeus, data: '0x' + contract_bytecode, gas: gasEstimate, gasPrice: 1000000000}, test_callback);
    function test_callback(error, transactionHash) {
        if(error) {
            console.log("    Error calling test callback: " + error);
            done();
        }
        else {
            console.log("    Deploying...\n    Transaction hash: " + transactionHash);
            var setup_filter = web.eth.filter('latest', function(error, result){
                if (!error) {
                    var contract_receipt = web.eth.getTransactionReceipt(transactionHash);
                    if (contract_receipt) {
                        var contract_address = contract_receipt.contractAddress;
                        if (contract_address) {
                            console.log("    Setting up...\n    Contract address: " + contract_address + "\n");
                            contract_instance = test_contract.at(contract_address);
                            setup_filter.stopWatching();
                            //TODO: FIXME: save started block time stamp. Make it accessible to tests
                            done();
                        }
                    }
                } else {
                    console.error(error);
                }
            });
        }
    }
}

function test(contract_params, custom_tests) {
    describe(contract_name, function(done) {
        before( "setup", function(done) {
            this.timeout(0);
            contract_setup(done);
        });

        custom_tests();
    });
}

module.exports = {
    test: test
};
