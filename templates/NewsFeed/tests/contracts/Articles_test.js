// ===================
// Input
// ===================
node_address = process.argv[3].split("=")[1];
if ( typeof node_address === 'undefined' )
{
    console.log("Missing node address");
    return 1;
}

contract_name = process.argv[4].split("=")[1];
if ( typeof contract_name === 'undefined' )
{
    console.log("Missing contract name");
    return 1;
}

contract_file_path = "./" + contract_name + ".sol";

contract_param_1="0x620cbab1f950e38a964d02ddcf85ecfcbb9f468f";
contract_params=[contract_param_1];


// ===================
// Tests
// ===================
function assert_expected_base_data() {
    describe('owner', function() {
        it('expected to match', function() {
            assert.equal("0x8833af1a9b1e7ddc837ed2c771078dcf3ac23daf", contract_instance.owner());
        });
    });

    describe('author', function() {
        it('expected to match', function() {
            assert.equal("0x620cbab1f950e38a964d02ddcf85ecfcbb9f468f", contract_instance.author());
        });
    });
}

function assert_num_articles(index) {
    describe('numArticles', function() {
        it('expected to match', function() {
            assert.equal(index, contract_instance.numArticles());
        });
    });
}

function tests() {
    var title = "A_title";
    var hash = "A_hash";
    var gaslimit = 1000000;

    assert_expected_base_data();
    assert_num_articles("0");

    describe('try to publish as NOT author', function() {
        it('expected to fail', function(done) {
            var res = contract_instance.publish(title, hash, {from: "0x8833af1a9b1e7ddc837ed2c771078dcf3ac23daf", gas: gaslimit, gasPrice: 1}, function(error, result) {
                var filter = web.eth.filter('latest', function(filter_error, filter_result) {
                    if (!error) {
                        var receipt = web.eth.getTransactionReceipt(result);
                        if(receipt) {
                            assert.equal(gaslimit, receipt.gasUsed);
                            filter.stopWatching();
                            done();
                        }
                    }
                });
            });
        });
    });
    assert_num_articles("0");

    describe('try to publish as author', function() {
        it('expected to match', function(done) {
            var res = contract_instance.publish(title, hash, {from: "0x620cbab1f950e38a964d02ddcf85ecfcbb9f468f", gas: gaslimit, gasPrice: 1}, function(error, result) {
                var filter = web.eth.filter('latest', function(filter_error, filter_result) {
                    if (!error) {
                        var receipt = web.eth.getTransactionReceipt(result);
                        if(receipt) {
                            assert.notEqual(gaslimit, receipt.gasUsed);
                            filter.stopWatching();
                            done();
                        }
                    }
                });
            });
        });
    });
    assert_num_articles("1");

    describe('retrieve article index as owner', function() {
        it('expected to match title and hash', function() {
            var res = contract_instance.getArticle(0, {from: "0x8833af1a9b1e7ddc837ed2c771078dcf3ac23daf", gas: gaslimit, gasPrice: 1});
            assert.equal(res[0], title);
            assert.equal(res[1], hash);
        });
    });

    describe('retrieve article index as author', function() {
        it('expected to match title and hash', function() {
            var res = contract_instance.getArticle(0, {from: "0x620cbab1f950e38a964d02ddcf85ecfcbb9f468f", gas: gaslimit, gasPrice: 1});
            assert.equal(res[0], title);
            assert.equal(res[1], hash);
        });
    });

    describe('retrieve article index as someone else', function() {
        it('expected to match title and hash', function() {
            var res = contract_instance.getArticle(0, {from: "0xd0cfc0a5d683630731e088488500b3cede5d1f2a", gas: gaslimit, gasPrice: 1});
            assert.equal(res[0], title);
            assert.equal(res[1], hash);
        });
    });

    describe('retrieve invalid article index as someone else', function() {
        it('expected to fail', function(done) {
            var res = contract_instance.getArticle(1, {from: "0xd0cfc0a5d683630731e088488500b3cede5d1f2a", gas: gaslimit, gasPrice: 1}, function(error, result) {
                assert.notEqual(error, null);
                done();
            });
        });
    });

    assert_expected_base_data();
    assert_num_articles("1");

    describe('try to publish again as author', function() {
        it('expected to match', function(done) {
            var res = contract_instance.publish(title, hash, {from: "0x620cbab1f950e38a964d02ddcf85ecfcbb9f468f", gas: gaslimit, gasPrice: 1}, function(error, result) {
                var filter = web.eth.filter('latest', function(filter_error, filter_result) {
                    if (!error) {
                        var receipt = web.eth.getTransactionReceipt(result);
                        if(receipt) {
                            assert.notEqual(gaslimit, receipt.gasUsed);
                            filter.stopWatching();
                            done();
                        }
                    }
                });
            });
        });
    });
    assert_expected_base_data();
    assert_num_articles("2");

    describe('retrieve article index as someone else', function() {
        it('expected to match title and hash', function() {
            var res = contract_instance.getArticle(0, {from: "0x8833af1a9b1e7ddc837ed2c771078dcf3ac23daf", gas: gaslimit, gasPrice: 1});
            assert.equal(res[0], title);
            assert.equal(res[1], hash);
        });
    });
}

// ===================
// Entrypoint
// ===================
var devkit = require('/home/devkit/tools/devkit.js');
devkit.test(contract_params, tests);
