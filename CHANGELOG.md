# Space Module change log - web-sh/devkit

## [current]

+ Add `/account/sign/`

+ Add ethereumjs-tx to Dockerfile

+ Add `BUILD_ETHEREUM_EXTERNAL` implementation

+ Add `-e key` parameter to `/build/`

+ Add support to the following Ethereum networks:
kovan, ropsten, rinkeby, infuranet and mainnet

+ Add `WEB3_JSON_EXTRACT` as included dependency

+ Add support to custom gas limit and price to `ACCOUNT_SIGN` and `BUILD_ETHEREUM_EXTERNAL`

+ Add `/enter/` operation

+ Add support for running a single test (`-e name` option)

+ Add automatic gaslimit definition when none is provided

+ Add `title` parameter to `/init/`

+ Added pre-defined accounts (wallet) in `genesis` configuration

+ Add `extraflags` option to `/run/`

+ Add `Localhost` custom network to predefined list in _MyEtherWallet_

+ Add error checking when building `.js` project files

+ Add `/enter/projects/` operation

+ Add file explorer endpoints

+ Add _nginx_ proxy features

+ Add _nginx_ configuration update script

* Fix version text in the web application

* Change `bump_version` script to automatically update web app version

* Change `docker` dependency to require specific version 1.4.0

* Change `file` dependency to require specific version 1.2.1

* Fix explorer not working when running with custom port settings

* Update `/stop/` to check container is running and exists before doing the respective actions

* Fix bug affecting `/info/`, `/build/`, `/js/` and `/make/` operations: `env` parameters do not read custom values

* Unify deployment code

* Improve `BUILD_PROJECT` to check compiled content before proceeding

* Change `/compile/` to support single contract compilation (`-e contract` option)

* Change `/build/` identification from private key to wallets (`-e wallets` option)

* Change _Dappfile_ format

* Improved checking for updated contracts

* Restructure `/js/` to take `-e wallets` and the new _Dappfile_ into account

* Update `devkit.js` tool to automatically calculate gas usage for tests

* Update Dockerfile `FROM` _space_ version to 1.2.1

* Update _Remix_ to build 7013ed1

* Update _RemixD_ to version 0.1.5

* Fix Remix connection with localhost not working with custom port settings

* Update code documentation

* Fix bug that prevents running `/test/` against a single contract

* Improve `/compile/` to return an error when `-e contract` is invalid

* Change `/add_args/` to reuse existing inner `_ADD_ARGS` implementation

* Update `/init/` to check for the existance of `Dappfile` when cloning a project

* Change web application to catch exit code

* Allow custom `image` parameter in `/run/`

* Allow custom `image_version` parameter in `/run/`

* Set all container commands to be executed as separate non-privileged user mapped to host user id (`uid`)

* Improve file permissions settings

* Set _Space_ to install modules in a common directory shared between users of `space` group

* Modify _MyEtherWallet_ to limit network list to _Ropsten_, _Rinkeby_ and _Kovan_ test networks

* Update _NewsFeed_ template tests

* Set default timeout for `/test/`

- Remove all permissions settings operations directly performed by the module code

- Remove `remixd` options


## [0.9.2 - 2018-02-02]

* Update README

* Change `/run/` to operate in detached mode

* Change `/stop/` to gracefully shutdown services before exit

* Change `Make` and `Build` application actions to always rebuild

* Change explorer install in Dockerfile to fetch a specific commit hash

* Fix iframe src to consider a trailing slash


## [0.9.1 - 2018-01-25]

* Update `ETHEREUM_COUNT_FN_ARGS` to allow for no constructor

* Update web application


## [0.9.0 - 2018-01-24]

+ Add fund account to main.lua

* Update app with account funding interface


## [0.8.5 - 2018-01-23]

* Fix donate action in _RaiseToSummon_ template to handle transaction result correctly

* Fix bug in counting `.sol` constructor arguments

* Update donate action in _RaiseToSummon_ template to detect initialization

* Change default donation amount in _RaiseToSummon_ template

* Change geth command to consider `networkid` matching custom genesis `chainId`

* Update default _CORS_ configuration to allow _Chrome_ and _Firefox_ extensions (including _MetaMask_)


## [0.8.4 - 2018-01-23]

* Split up single-line local variables declaration to prevent masking return values

* Update install and run instructions to be more clear about firewall settings and `-e port` option

* Update web application


## [0.8.3 - 2018-01-21]

* Bump Docker image


## [0.8.2 - 2018-01-19]

* Change telegram URL in web application


## [0.8.1 - 2018-01-19]

* Update `INSTALL` instructions

* Update code documentation

* Change telegram URL in web application


## [0.8.0 - 2018-01-18]

+ Add initial tests to _NewsFeed_ template project

+ Add `- account: USER1` to Dappfile

* Change `PROJECT_INIT` to consider `umask`

* Fix permissions settings in `/init/`

* Change project arguments to deploy directory for viewing

* Update project templates

* Update default _CORS_ configuration to consider _MetaMask_

* Update web application


## [0.7.0 - 2018-01-17]

+ Add _Lua_ to _nginx_

+ Add web application

+ Add `/info/`

+ Add `-e apis` to `/run/`

+ Add `/events/`

* Solve various static analysis warnings

* Update module help text information and description

* Fix typo in module help text information

* Rename default home project directory

* Rename default container name

* Fix bug for exporting JS

* Update code documentation

* Update `/add` templates

* Allow `/test/` to use `./contracts` as default directory


## [0.6.0 - 2018-01-15]

+ Add INSTALL file

+ Add pre-installed DevKit Space module to Dockerfile

+ Add error checking to `BUILD_ETHEREUM_LOCAL`

* Change _Block Explorer_ relative links to consider /explorer/ server route

* Update instructions on README

* Solve various static analysis warnings

* Update code documentation

* Update README to include install instructions

* Fix wrong path for explorer configuration in Dockerfile

* Modify _YAML_ to not allow `RUN` parameters to be overridden


## [0.5.2 - 2018-01-11]

* Move `remixd` initialization from Dockerfile to entrypoint.sh

* Move _Block Explorer_ initialization from Dockerfile to entrypoint.sh


## [0.5.1 - 2018-01-11]

* Fix wrong path used for Remix configuration


## [0.5.0 - 2018-01-11]

+ Include /add_args/

+ Add timestamp feature in `/compile/` (skip rebuild by default)

+ Add `-e recompile` option to `/compile/`

+ Add support for Dapp specification file: `Dappfile.yaml`

+ Add support for accessing shared projects directory directly in the _Remix IDE_ with _Remixd_

+ Add `-e remixdport` option to `/run/`

+ Add `COMPILE_ETHEREUM` function

+ Add `DAPPFILE_CONTRACT` functions

+ Add `BUILD_WAIT` function

+ Add `BUILD_ETHEREUM` functions

+ Add `/js/`

+ Add `/stop/`

+ Add `/make/`

+ Add ability for contracts to reside in sub-directories

+ Add `-e rebuild` option to `/build/`

+ Add block explorer

* Change single line commands to proper if construct

* Update documentation

* Move abi generation from `/compile/` to `WEB_BUILD` step

* Move js helper files from `/compile/` to `WEB_BUILD` step

* Solve various static analysis warnings

* Change `break` keyword to non-zero `return` in `/add_args/`

* Update `/compile/` to consider `Dappfile.yaml` specification

* Change module file extension from `.sh` to `.bash`

* Change `stat` command in favor of `FILE_STAT`

* Change all `exit` statements to `return` keyword

* Rename `COMPILE` to `COMPILE_PROJECT`

* Change `COMPILE` implementation to support Dappfile specification

* Change `BUILD` implementation to support Dappfile specification

* Double quote condition parameters to prevent word splitting

* Split up single-line local variables declaration to prevent masking return values

* Reorganize project target structure

* Update _MyEtherWallet_ (MEW) to version 3.11.2.1

* Rename `/deploy/` to `/build/`

* Modify `/init/` to work with multiple templates

* Rename default template from `dapp` to `raiseforacause`

* Update Remix to build d50dcc4

- Remove arguments support from `/compile/`

- Remove `ipc` argument from /compile/

- Remove `contract` argument from /compile/

- Remove `contract` argument from /build/

- Remove `gas` argument from /build/

- Remove `account` argument from /build/


## [0.4.2 - 2017-12-26]

* Fix shebang lines for all tools


## [0.4.1 - 2017-12-26]

* Update _MyEtherWallet_ (MEW) to version 3.11.1.2

* Update _Solidity_ compiler (solc) to version 0.4.19

* Update Remix to build f4e1f6c


## [0.4.0 - 2017-12-26]

+ Add `hotreload` option to `/test/`

+ Add `breakonfailure` option to `/test/`

+ Add improved error checking in `/deploy/`

+ Add `-e optimization` switch to `/compile/`

+ Add optional `-e arguments` to `/compile/` for appending constructor arguments

+ Add initial local testing

+ Add `jq` program to Dockerfile

+ Add `nginx` parameter to `/run/`

+ Add `Spacefile` to dapp template

+ Add check in `/compile/` to verify arguments count match expected constructor signature

+ Add CI tests

+ Add external programs availability check

+ Add `SPACE_ENV` setting for external programs

* Set `/test/` lower bound threshold to minimum value to always output execution time

* Change `/deploy/` control flow to exit with error when `.abi` or `.bin` files are missing

* Improve `contract_bin` definition in `/deploy/` to only prefix `0x` when needed

* Separate commands containing declaration and assignment on the same line

* Update documentation

* Change all target operations to work outside of container

* Update `/compile/` to create www/js directory when needed

* Fix bug that happens when setting the projects home directory

* Update instructions on README

* Fix module description typo

- Disable false positive shellcheck warnings

- Remove `ethereumjs-testrpc`

- Remove `docker` as dependency in `dep_install`


## [0.3.1 - 2017-11-28]

* Fix potential word splitting variable expansion

* Update `/test/` to use existing `.abi` and `.bin` files when available

* Move _solc_ js package out of the fast `devkit.js` code path

* Update Dockerfile for improved readability

* Update Dockerfile to explicitly declare build-only dependencies as virtual

* Update Dockerfile to explicitly ignore caching

- Remove unused variables


## [0.3.0 - 2017-11-27]

+ Add `/test/`

+ Add `devkit.js` tool for testing

+ Add tests for dapp template

* Fix bug that prevents empty or default genesis on `/run/`

* Update instructions on README

* Change Dockerfile to include `mocha` for testing

* Double quote condition parameters to prevent word splitting

- Disable false positive shellcheck warnings


## [0.2.0 - 2017-11-23]

+ Add `bump_version` helper script

+ Add `-e genesis` option for custom genesis file on `/run/`

+ Add support for `-e cors` option for configurable CORS policy

+ Add support for `-e corsip` option for configurable host IP address (CORS policy)

* Change preloaded list of Geth APIs to include `net` and `txpool`

* Change Dockerfile entrypoint to give included tools more flexibility (parametrization)

* Change default `cors` variable to include common `192.168.99.100` docker-machine IP.

- Remove bashisms in favor of sh/dash equivalents


## [0.1.1 - 2017-11-22]

* Update code documentation

* Update `geth` to version 1.7.3


## [0.1.0 - 2017-11-21]

+ Add working openresty

* General improvements to Space module

* General improvements to DApp template


## [0.0.6 - 2017-11-20]

+ Add code documentation

+ Add Ethereum units conversion helper

+ Add `/_dep_install/`

+ Add explicit `os` module dependency

* Update dapp template

* Update README

* Update Dockerfile to include openresty


## [0.0.5 - 2017-11-17]

+ Add `image_version` environment variable

* Fix template contract constructor


## [0.0.4 - 2017-11-15]

* Improve several functions and services

* Update instructions

* Optimize Docker image for size


## [0.0.3 - 2017-11-15]

* Update _MyEtherWallet_ (MEW) to version 3.10.7.1

* Update _Solidity_ compiler (solc) to version 0.4.18


## [0.0.2 - 2017-11-15]

+ Add space-requirement settings

* Change `Dockerfile` to have better prepare for software version updates


## [0.0.1 - 2017-11-15]

+ Initial version
