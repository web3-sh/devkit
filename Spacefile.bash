#
# Copyright 2017 Blockie AB
# Copyright 2018 Superblocks AB
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#=====================
# DEVKIT_DEP_INSTALL
#
# Check dependencies for this module.
#
# Returns:
#   0: dependencies were found
#   1: failed to find dependencies
#
#=====================
DEVKIT_DEP_INSTALL()
{
    SPACE_DEP="OS_IS_INSTALLED PRINT"       # shellcheck disable=SC2034

    PRINT "Checking for DEVKIT dependencies." "info"

    if OS_IS_INSTALLED "bc" "bc" && OS_IS_INSTALLED "sed" "sed" && OS_IS_INSTALLED "cut" "cut"; then
        PRINT "Dependencies found." "ok"
    else
        PRINT "Failed finding dependencies." "error"
        return 1
    fi
}


# Disable false positive about unused MODULEDIR
# shellcheck disable=SC2034
# shellcheck disable=SC2154

#=============
# DEVKIT_PROJECT_INIT
#
# Initialize a new project
#
# Parameters:
#   $1: template directory
#   $2: projects directory
#   $3: project name
#
# Expects:
#   MODULEDIR
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_PROJECT_INIT()
{
    SPACE_SIGNATURE="template:0 projectsdir project title:0"
    SPACE_DEP="PRINT"
    SPACE_ENV="MODULEDIR"

    local template="${1:-blank}"
    shift

    local projectsdir="${1}"
    shift

    local project="${1}"
    shift

    local title="${1}"
    shift

    if [[ ! ${project} =~ ^([a-zA-Z0-9_-]+)$ ]]; then
        PRINT "Illegal project name. Only [A-Za-z0-9_-] are allowed." "error"
        return 1
    fi

    if ! mkdir -p "${projectsdir}"; then
        PRINT "Could not create/find projects directory ${projectsdir}" "error"
        return 1
    fi

    local projectdir="${projectsdir}/${project}/"
    if [ -e "${projectdir}/" ]; then
        PRINT "Project ${project} already exists, please choose a different name for your project." "error"
        return 1
    fi

    if [[ ${template} =~ ^([a-zA-Z0-9_-]+)$ ]]; then
        local templatedir="${MODULEDIR}/templates/${template}"
        PRINT "Using DApp template ${template}."
        if [ ! -d "${templatedir}" ]; then
            PRINT "Template ${template} not found." "error"
            return 1
        fi
        cp -r "${templatedir}/." "${projectdir}/"
    else
        # Perform git clone to init project
        PRINT "Clone git repo ${template}"
        if ! git clone "${template}" "${projectdir}/"; then
            PRINT "Could not clone template git repo." "error"
            return 1
        fi
        # Remove .git folder
        rm -rf "${projectdir}/.git"
    fi
    local dappfile_path="${projectdir}/Dappfile.yaml"

    if [ ! -f "${dappfile_path}" ]; then
        PRINT "No Dappfile.yaml found in cloned project ${project}, removing it." "error"
        # Remove cloned project
        rm -rf "${projectdir}"
        return 1
    fi

    # Update Dappfile.yaml with given project title.
    local data=
    data=$(cat "${dappfile_path}")

    local data2=
    data2=$(printf "%s\\n" "${data}" | luajit -e "
lyaml=require'lyaml';
local t=lyaml.load(io.read('*all'));
local title='${title}';

if #title>0 then
    t.project = t.project or {}
    local p=t.project
    p.info=p.info or {}
    p.info.title=title;
end
print(lyaml.dump({t}))

")
    printf "%s\\n" "${data2}" > "${dappfile_path}"

    PRINT "Project init in ${projectdir}" "ok"
}

#=============
# DEVKIT_RUN_CONTAINER
#
# Run Docker container
#
# Parameters:
#   $1: container image
#   $2: container name
#   $3: projects directory
#   $4: web server port
#   $5: RPC port
#   $6: WS port
#   $7: remixd port
#   $8: CORS policy (optional)
#   $9: genesis configuration file (optional)
#   $10: path to nginx directory (optional)
#
# Returns:
#   Non-zero on failure.
#
#=================
DEVKIT_RUN_CONTAINER()
{
    SPACE_SIGNATURE="image container projectsdir port rcpport wsport [cors genesis nginx apis extraflags]"
    SPACE_DEP="PRINT NETWORK_PORT_BUSY DOCKER_RUN STRING_SUBST DOCKER_EXIST"

    local image="${1}"
    shift

    local container="${1}"
    shift

    local projectsdir="${1}"
    shift

    local port="${1}"
    shift

    local rpcport="${1}"
    shift

    local wsport="${1}"
    shift

    local cors="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    local genesis="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    local nginx="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    local apis="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    local extraflags="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    # Trim away :80 on cors.
    STRING_SUBST "cors" ':80,' ',' 1
    cors="${cors%:80}"

    if ! command -v "docker" >/dev/null; then
        PRINT "Unable to find docker executable" "error"
        return 1
    fi

    if ! mkdir -p "${projectsdir}"; then
        PRINT "Could not create/find projects directory ${projectsdir}" "error"
        return 1
    fi

    # Make absolute
    projectsdir=$( cd "${projectsdir}" && pwd )

    # Enable absolute and relative path for genesis
    local genesis_full_path=""
    local genesisdir=${genesis%/*}
    genesis=${genesis##*/}
    if [ -z "$genesisdir" ] || [ "$genesisdir" = "$genesis" ]; then
        genesis_full_path="$PWD/$genesis"
        genesisdir=${genesis_full_path%/*}
    else
        genesisdir=$( cd "$genesisdir" && pwd )
        genesis_full_path="${genesisdir}/${genesis}"
    fi

    local nginx_full_path=
    if [ -n "${nginx}" ]; then
        nginx_full_path=$( cd "${nginx}" && pwd )
    fi

    PRINT "Genesis: $genesis | Genesisdir: $genesisdir | Genesis full path: $genesis_full_path" "debug"
    if [ -n "${genesis}" ] && [ ! -f "${genesis_full_path}" ]; then
        PRINT "Genesis file not found: ${genesis_full_path}" "error"
        return 1
    fi

    # Check if ports are busy already.
    if NETWORK_PORT_BUSY "${port}"; then
        PRINT "The HTTP port ${port} is already in use. Kill that process or change the port using '-e port='" "error"
        return 1
    fi
    if NETWORK_PORT_BUSY "${rpcport}"; then
        PRINT "The RPC port ${rpcport} is already in use. Kill that process or change the port using '-e rpcport='" "error"
        return 1
    fi
    if NETWORK_PORT_BUSY "${wsport}"; then
        PRINT "The RPC port ${wsport} is already in use. Kill that process or change the port using '-e wsport='" "error"
        return 1
    fi
    # This is set to one space to have the docker module not apply sh as command, since we want the docker image entrypoint to run.
    local emptycmd=" "

    if DOCKER_EXIST "${container}"; then
        PRINT "The docker container ${container} already exists. Run /stop/ to kill it, then try again." "error"
        return 1
    fi

    # Ports 5001, 8080 are ipfs.

    DOCKER_RUN "${image}" "${container}" "--restart=always -d -p ${port}:80 -p ${rpcport}:8545 -p ${wsport}:8546 -p 5001:5001 -p 8080:8080 -v ${projectsdir}:/home/devkit/projects ${genesis:+-v ${genesis_full_path}:/home/devkit/geth/genesis.json} ${nginx:+-v ${nginx_full_path}:/home/devkit/nginx} ${extraflags}" "${emptycmd}" "${cors}" "${apis}"
}

#=============
# DEVKIT_STOP_CONTAINER
#
# Stop Docker container
#
# Parameters:
#   $1: container name
#
# Returns:
#   Non-zero on failure.
#
#=================
DEVKIT_STOP_CONTAINER()
{
    SPACE_SIGNATURE="container"
    SPACE_DEP="DOCKER_EXEC DOCKER_PS DOCKER_RM_BY_ID DOCKER_STOP"

    local container="${1}"
    shift

    # Gracefully shutdown services
    local container_id=
    container_id=$(DOCKER_PS "-q" "-f" "status=running" "-f name=^/${container}$")
    if [ "$container_id" ]; then
        DOCKER_EXEC "${container}" "-d" "pkill -SIGINT openresty"
        DOCKER_EXEC "${container}" "-d" "pkill -SIGINT node"
        DOCKER_EXEC "${container}" "-d" "pkill -SIGINT geth"
    fi

    # Remove the container
    container_id=
    container_id=$(DOCKER_PS "-a" "-q" "-f name=^/${container}$")
    if [ "$container_id" ]; then
        DOCKER_STOP "${container}"
        DOCKER_RM_BY_ID "${container}"
    fi
}

#=============
# DEVKIT_CONSOLE
#
# Open console to geth
#
# Parameters:
#   $1: Geth IPC address
#
# Expects:
#   GETH to point to go-ethereum, defaults to "geth".
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_CONSOLE()
{
    SPACE_SIGNATURE="ipc"
    SPACE_ENV="GETH=\"${GETH:-geth}\""

    local ipc="${1}"
    shift

    if ! command -v "${GETH}" >/dev/null; then
        PRINT "Unable to find geth executable at: ${GETH}" "error"
        return 1
    fi

    ${GETH} attach "${ipc}"
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#=============
# DEVKIT_ACCOUNT_TRANSFER
#
# Transfer funds from one account to another.
#
# Parameters:
#   $1: ipc address
#   $2: From address
#   $3: To address
#   $4: value in wei
#
# Expects:
#   GETH to point to go-ethereum, defaults to "geth".
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_ACCOUNT_TRANSFER()
{
    SPACE_SIGNATURE="ipc from to value"
    SPACE_ENV="GETH=\"${GETH:-geth}\""

    local ipc="${1}"
    shift

    local from="${1}"
    shift

    local to="${1}"
    shift

    local value="${1}"
    shift

    if ! command -v "${GETH}" >/dev/null; then
        PRINT "Unable to find geth executable at: ${GETH}" "error"
        return 1
    fi

    local cmd='eth.sendTransaction({"from": "'${from}'", "to": "'${to}'", "value": "'${value}'"});'
    local output=
    output=$(${GETH} attach "${ipc}" --exec "${cmd}")
    if [ $? -gt 0 ]; then
        return 1
    fi
    printf "%s\n" "${output}" | sed 's/"//g'
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#=============
# DEVKIT_ACCOUNT_LIST
#
# List available accounts.
#
# Parameters:
#   $1: ipc address
#
# Expects:
#   GETH to point to go-ethereum, defaults to "geth".
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_ACCOUNT_LIST()
{
    SPACE_SIGNATURE="ipc"
    SPACE_ENV="GETH=\"${GETH:-geth}\""

    local ipc="${1}"
    shift

    if ! command -v "${GETH}" >/dev/null; then
        PRINT "Unable to find geth executable at: ${GETH}" "error"
        return 1
    fi

    local cmd='eth.accounts.forEach(function(l) { console.log(l) }) || 0'
    local output=
    output=$(${GETH} attach "${ipc}" --exec "${cmd}")
    if [ $? -gt 0 ]; then
        return 1
    fi
    printf "%s\n" "${output}" | sed '/0$/d'
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#=============
# DEVKIT_ACCOUNT_SIGN
#
# Sign a transaction
#
# Parameters:
#   $1: network host address
#   $2: Chain ID
#   $3: from address
#   $4: to address
#   $5: transaction data
#   $6: transaction value
#   $7: account private key
#   $8: gas price
#   $9: gas limit
#
# Expects:
#   NODE to point to node.js, defaults to "node".
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_ACCOUNT_SIGN()
{
    SPACE_SIGNATURE="host chainid from to data:0 value:0 key gasprice gaslimit"
    SPACE_ENV="NODE=\"${NODE:-node}\""

    local host="${1}"
    shift

    local chainid="${1}"
    shift

    local from="${1}"
    shift

    local to="${1}"
    shift

    local data="${1}"
    shift

    local value="${1}"
    shift

    local key="${1}"
    shift

    local gasprice="${1}"
    shift

    local gaslimit="${1}"
    shift

    if ! command -v "${NODE}" >/dev/null; then
        PRINT "Unable to find node executable at: ${NODE}" "error"
        return 1
    fi

    local output=
    # shellcheck disable=2086
    output=$(${NODE} -e '
const web3 = require("web3");
const eth_tx = require("ethereumjs-tx");
const web = new web3(new web3.providers.HttpProvider("'${host}'"));
const account_nonce = web.eth.getTransactionCount("'${from}'");
const tx_params = {
    chainId: '${chainid}',
    nonce: account_nonce,
    gasPrice: '${gasprice}',
    gasLimit: '${gaslimit}',
    to: "'${to}'",
    value: "'${value}'",
    data: "'${data}'",
};
const tx = new eth_tx(tx_params);
tx.sign(Buffer.from("'${key}'", "hex"));
const tx_raw = "0x" + tx.serialize().toString("hex");
console.log(tx_raw);
')
    if [ $? -gt 0 ]; then
        return 1
    fi
    printf "%s\n" "${output}"
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#=============
# DEVKIT_ACCOUNT_SEED
#
# Generate a random 12-word seed
#
# Parameters:
#   None
#
# Expects:
#   NODE to point to node.js, defaults to "node".
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_ACCOUNT_SEED()
{
    SPACE_SIGNATURE=""
    SPACE_ENV="NODE=\"${NODE:-node}\""

    if ! command -v "${NODE}" >/dev/null; then
        PRINT "Unable to find node executable at: ${NODE}" "error"
        return 1
    fi

    local output=
    output=$(${NODE} -e '
var lightwallet = require("eth-lightwallet");
console.log(lightwallet.keystore.generateRandomSeed());
')
    if [ $? -gt 0 ]; then
        return 1
    fi
    printf "%s\n" "${output}"
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#=============
# DEVKIT_COMPILE_PROJECT
#
# Compile a smart contract.
#
# Parameters:
#   $1: projects home directory
#   $2: project name
#   $3: optimization flag
#   $4: recompile flag
#   $5: contract name
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_COMPILE_PROJECT()
{
    SPACE_SIGNATURE="projectshome project optimization:0 recompile:0 contract:0"
    SPACE_DEP="PRINT STRING_ITEM_GET _DEVKIT_COMPILE_ETHEREUM _DEVKIT_DAPPFILE_CONTRACTS"

    local projectshome="${1}"
    shift

    local project="${1}"
    shift

    local optimization="${1}"
    shift

    local recompile="${1}"
    shift

    local contract_to_compile="${1}"
    shift

    local projectdir="${projectshome}/${project}"

    # For each contract in Dappfile
    local dappfile_path="${projectdir}/Dappfile.yaml"

    if [ ! -f "${dappfile_path}" ]; then
        PRINT "No Dappfile.yaml found in project ${project}." "error"
        return 1
    fi

    local compile_counter=0
    local line=
    while read -r line; do
        local _ifs="${IFS}"
        IFS=';'
        local contract=
        STRING_ITEM_GET "${line}" 0 "contract"
        local source=
        STRING_ITEM_GET "${line}" 1 "source"
        local blockchain=
        STRING_ITEM_GET "${line}" 3 "blockchain"
        IFS="${_ifs}"
        if [ -n "${contract_to_compile}" ] && [ "${contract_to_compile}" != "${contract}" ]; then
            continue
        fi
        if [ -z "${source}" ]; then
            PRINT "No source defined for ${contract}, skipping compile." "warning"
            continue
        fi
        if [ "${blockchain}" != "ethereum" ]; then
            PRINT "Blockchain $blockchain for contract $contract not supported, skipping." "error"
            continue
        fi
        if [[ ! ${contract} =~ ^([a-zA-Z0-9_-]+)$ ]]; then
            PRINT "Contract name contains illegal characters or is empty: ${contract}" "error"
            return 1
        fi
        if ! _DEVKIT_COMPILE_ETHEREUM "${projectdir}/${source}" "${contract}" "${optimization}" "${recompile}"; then
            return 1
        else
            compile_counter=$((compile_counter + 1 ))
        fi
    done < <(_DEVKIT_DAPPFILE_CONTRACTS "${dappfile_path}" "" "-1")
    #      ^ Bashism

    if [ "${compile_counter}" -eq 0 ] && [ -n "${contract_to_compile}" ]; then
        PRINT "Failed to find contract: ${contract_to_compile}" "error"
        return 1
    else
        return 0
    fi
}

#=========================
# _DEVKIT_COMPILE_ETHEREUM
#
# Compile for Ethereum
#
# Parameters:
#   $1: source file path
#   $2: contract name
#   $3: optimization flag
#   $4: recompile flag
#
# Expects:
#   SOLC to point to solidity compiler, defaults to "solc".
#
# Returns:
#   Non-zero on error
#
#=========================
_DEVKIT_COMPILE_ETHEREUM()
{
    SPACE_SIGNATURE="sourcefile contract optimization:0 recompile:0"
    SPACE_DEP="PRINT FILE_STAT"
    SPACE_ENV="SOLC=\"${SOLC:-solc}\""

    local sourcefile="${1}"
    shift

    local contract="${1}"
    shift

    local optimization="${1}"
    shift

    local recompile="${1}"
    shift

    local sourcedir="${sourcefile%/*}"
    local shortfile="${sourcefile##*/}"

    if [ "${shortfile##*.}" != "sol" ]; then
        PRINT "${shortfile} is not recognized. DevKit can only compile Solidity (.sol) contracts for Ethereum. " "error"
        return 1
    fi

    if [ ! -f "${sourcefile}" ]; then
        PRINT "${shortfile} is not found." "error"
        return 1
    fi

    if ! command -v "${SOLC}" >/dev/null; then
        PRINT "Unable to find solc executable at: ${SOLC}" "error"
        return 1
    fi

    # Check timestamp of previous compilation.
    # TODO: This will fail to trigger a recompile when only 'imported' libraries have been updated.
    local timestamp_file="${sourcedir}/.${contract}.ts"
    local current_timestamp=
    current_timestamp=$(FILE_STAT "${sourcefile}" "%Y")
    if [ "${recompile}" != "true" ] && [ -f "${timestamp_file}" ] && [ -f "${sourcedir}/${contract}.abi" ] && [ -f "${sourcedir}/${contract}.bin" ]; then
        local timestamp=
        timestamp=$(cat "${timestamp_file}")
        if [ -n "${timestamp}" ] && [ "${current_timestamp}" -le "${timestamp}" ]; then
            PRINT "Skipping already compiled contract: ${contract}" "info"
            return 0
        fi
    fi

    PRINT "Compiling ${shortfile}:${contract}" "info"

    # Create a temporary location for compiled contracts.
    # We do this so we can control and verify the output of the compiler.
    local tmpdir=
    tmpdir=$(mktemp -d 2>/dev/null || mktemp -d -t 'sometmpdir')

    if [ -z "${tmpdir}" ]; then
        PRINT "Could not proceed with compilation due to failing in creating temporary directory." "error"
        return 1
    fi

    local status=0
    while true; do
        local flags=""
        if [ "${optimization}" = "true" ]; then
            flags="${flags} --optimize"
        fi
        PRINT "Bytecode optimization: ${optimization}" "info"

        # Note: intentionally missing quotes around flags
        # shellcheck disable=2086
        if ! ${SOLC} "${sourcefile}" ${flags} -o "${tmpdir}" --bin 2>&1; then
            PRINT "Could not compile contract." "error"
            status=1
            break
        fi
        # shellcheck disable=2086
        if ! ${SOLC} "${sourcefile}" ${flags} -o "${tmpdir}" --abi 2>&1; then
            PRINT "Could not compile contract." "error"
            status=1
            break
        fi

        # Check the existence of the anticipated files.

        local binfile="${tmpdir}/${contract}.bin"
        if [ ! -f "${binfile}" ]; then
            PRINT "Was expecting ${binfile} to have been created by compiler." "error"
            status=1
            break
        fi
        local abifile="${tmpdir}/${contract}.abi"
        if [ ! -f "${abifile}" ]; then
            PRINT "Was expecting ${abifile} to have been created by compiler." "error"
            status=1
            break
        fi

        PRINT "Compile ${shortfile}:${contract} succeeded." "info"

        # Copy the created files back to source dir and update timestamp file.
        if ! cp -p "${binfile}" "${sourcedir}"; then
            PRINT "Could not copy contract binary to project directory." "error"
            status=1
            break
        fi
        if ! cp -p "${abifile}" "${sourcedir}"; then
            PRINT "Could not copy contract ABI to project directory." "error"
            status=1
            break
        fi

        printf "%s\n" "${current_timestamp}" > "${timestamp_file}"
        break
    done

    # Cleanup
    rm -rf "${tmpdir}"
    return ${status}
}

#=============
# DEVKIT_INFO_PROJECT
#
# Get info on smart contracts for the project.
#
# Parameters:
#   $1: Geth IPC address
#   $2: projects home directory
#   $3: project name
#   $4: blockchain environment name
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_INFO_PROJECT()
{
    SPACE_SIGNATURE="ipc projectshome project env"
    # shellcheck disable=SC2034
    SPACE_DEP="PRINT STRING_ITEM_GET _DEVKIT_QUOTE_ARG"

    local ipc="${1}"
    shift

    local projectshome="${1}"
    shift

    local project="${1}"
    shift

    local env="${1}"
    shift

    if [[ ! ${env} =~ ^([a-zA-Z0-9_]+)$ ]]; then
        return 2
    fi

    local projectdir="${projectshome}/${project}"

    if [ ! -d "${projectdir}/deploy/local" ]; then
        PRINT "No deployments found."
        return
    fi

    # TODO: Make this more competent as in looking in the Dappfile to see which contracts should
    # be deployed.
    # Now it's a really simple.

    local wildcard="${projectdir}/deploy/local/*.*.*.*.*.addr"
    local file=
    for file in $wildcard; do
        if [ ! -f "${file}" ]; then
            PRINT "Could not find deployments"
            return 1
        fi
        file="${file##*/}"
        if [[ ${file} =~ ^([^.]+).([^.]+).([^.]+).([^.]+).([^.]+).addr ]]; then
            local contract="${BASH_REMATCH[1]}";
            local blockchain="${BASH_REMATCH[2]}";
            local network="${BASH_REMATCH[3]}";
            local account="${BASH_REMATCH[4]}";
            local account_address="${BASH_REMATCH[5]}";
            local address=
            address=$(cat "${projectdir}/deploy/local/${file}")
            local arguments=
            arguments=$(cat "${projectdir}/deploy/local/${file%.addr}.args")
            printf "%s deployed at address %s by account %s (address: %s) with arguments:\\n%s\\n" "${contract}" "${address}" "${account}" "${account_address}" "${arguments}"
        else
            PRINT "Unknown format of file name ${file}." "warning"
        fi
            #Articles.ethereum.local.DEPLOY.0x8833af1a9b1e7ddc837ed2c771078dcf3ac23daf.addr
    done
}

# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#=============
# DEVKIT_BUILD_PROJECT
#
# Build a smart contract to the network.
#
# Parameters:
#   $1: Geth IPC address
#   $2: projects home directory
#   $3: project name
#   $4: blockchain environment name
#   $5: rebuild flag
#   $6: wallets for signing
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_BUILD_PROJECT()
{
    SPACE_SIGNATURE="ipc projectshome project env rebuild wallets:0"
    # shellcheck disable=SC2034
    SPACE_DEP="PRINT STRING_ITEM_GET _DEVKIT_QUOTE_ARG _DEVKIT_BUILD_WAIT _DEVKIT_DAPPFILE_CONTRACTS _DEVKIT_DAPPFILE_CONTRACT_ARGS _DEVKIT_BUILD _DEVKIT_ETHEREUM_COUNT_FN_ARGS _DEVKIT_DAPPFILE_ACCOUNT"

    local ipc="${1}"
    shift

    local projectshome="${1}"
    shift

    local project="${1}"
    shift

    local env="${1}"
    shift

    local rebuild="${1}"
    shift

    local wallets="${1}"
    shift

    if [[ ! ${env} =~ ^([a-zA-Z0-9_]+)$ ]]; then
        return 2
    fi

    local projectdir="${projectshome}/${project}"

    if [ ! -d "${projectdir}" ]; then
        PRINT "Could not find project directory for ${project}." "error"
        return 1
    fi

    local dappfile_path="${projectdir}/Dappfile.yaml"
    if [ ! -f "${dappfile_path}" ]; then
        PRINT "No Dappfile.yaml found in project ${project}." "error"
        return 1
    fi

    # Check so that every compiled contract exists before building any.
    while read -r line; do
        local _ifs="${IFS}"
        IFS=';'
        local contract=
        STRING_ITEM_GET "${line}" 0 "contract"
        local source=
        STRING_ITEM_GET "${line}" 1 "source"
        local blockchain=
        STRING_ITEM_GET "${line}" 3 "blockchain"
        local nrargs=0
        STRING_ITEM_GET "${line}" 8 "nrargs"

        IFS="${_ifs}"
        if [[ ! ${contract} =~ ^([a-zA-Z0-9_-]+)$ ]]; then
            PRINT "Contract name contains illegal characters or is empty: ${contract}" "error"
            return 1
        fi

        local sourcefile="${projectdir}/${source}"
        if [ ! -f "${sourcefile}" ]; then
            PRINT "Source file $sourcefile for contract $contract not found." "error"
            return 1
        fi

        # Check number of arguments in constructor
        if [ "${blockchain}" = "ethereum" ]; then
            local nrargs2=
            nrargs2=$(_DEVKIT_ETHEREUM_COUNT_FN_ARGS "${sourcefile}" "${contract}")
            if [ "${nrargs}" -ne "${nrargs2}" ]; then
                PRINT "Wrong argument count for constructor: ${contract}. Expected: ${nrargs2}. Got: $nrargs" "error"
                return 1
            fi
        else
            PRINT "Unknown blockchain: ${blockchain} for contract: ${contract}. Hint: use lower case." "error"
            return 1
        fi

        # External: dirname
        local contractdir=
        contractdir="${projectdir}/$(dirname "${source}")"
        # TODO: This only checks Ethereum contracts for now, we need to extend that when we add support for more blockchains.
        if [ ! -f "${contractdir}/${contract}.bin" ] || [ ! -f "${contractdir}/${contract}.abi" ]; then
            PRINT "The contract ${contract} must be compiled before it can be built" "error"
            return 1
        fi
    done < <(_DEVKIT_DAPPFILE_CONTRACTS "${dappfile_path}" "${env}" "-1")

    local ts=
    ts=$(date "+%s")
    local deploydir="${projectdir}/deploy/.intermediate.${env}.${ts}"
    local finaldir="${projectdir}/deploy/${env}"

    PRINT "Building intermediary into ${deploydir}, final dest: ${finaldir}." "debug"

    mkdir -p "${deploydir}" || {
        PRINT "Can't create deploydir ${deploydir}." "error"
        return 1
    }

    # Used by nested functions.
    local _deploy_pending=()
    local _deploy_done=()

    # For each contract in Dappfile
    local index=0
    while true; do
        local line=
        line=$(_DEVKIT_DAPPFILE_CONTRACTS "${dappfile_path}" "${env}" "${index}")
        if [ -n "${line}" ]; then
            local contract=
            local source=
            local account=
            local blockchain=
            local network=
            local gaslimit=
            local _ifs="${IFS}"
            IFS=';'
            STRING_ITEM_GET "${line}" 0 "contract"
            STRING_ITEM_GET "${line}" 1 "source"
            STRING_ITEM_GET "${line}" 2 "account"
            STRING_ITEM_GET "${line}" 3 "blockchain"
            STRING_ITEM_GET "${line}" 4 "network"
            STRING_ITEM_GET "${line}" 5 "gaslimit"
            IFS="${_ifs}"
            local out_address=
            local out_privkey=
            if ! _DEVKIT_DAPPFILE_ACCOUNT "${dappfile_path}" "${env}" "${account}" "${blockchain}" "${wallets}"; then
                PRINT "Could not find address for account ${account}." "error"
                return 1
            fi
            local address="${out_address}"
            local privkey="${out_privkey}"
            local arguments=
            local arg=
            while read -r arg; do
                if [ "${arg#contract:}" != "${arg}" ]; then
                    arg="${arg#contract:}"
                    # Argument is referring to a deployed contract address.
                    # We'll wait for any contracts pending deployment before proceeding.
                    if ! _DEVKIT_BUILD_WAIT; then
                        return 1
                    fi
                    # Get the address
                    local address_file="${deploydir}/${arg}"
                    # Note: Unquoted on purpose (address_file)
                    # shellcheck disable=2086
                    if [ -f ${address_file} ]; then
                        local argv=
                        argv=$(cat ${address_file})
                        if [ -z "${argv}" ]; then
                            PRINT "Could not get anticipated address of ${arg} which is used as argument to ${contract}. Check the building order of the contracts." "error"
                            return 1
                        fi
                        arg="${argv}"
                    else
                        PRINT "Could not get anticipated address of ${arg} which is used as argument to ${contract}. Check the building order of the contracts." "error"
                        return 1
                    fi
                elif [ "${arg#account:}" != "${arg}" ]; then
                    arg="${arg#account:}"
                    # Argument is referring to an account address.
                    local out_address=
                    local out_privkey=
                    if ! _DEVKIT_DAPPFILE_ACCOUNT "${dappfile_path}" "${env}" "${arg}" "${blockchain}" "${wallets}"; then
                        PRINT "Could not find address for account ${arg} referred to as argument for contract ${contract}." "error"
                        return 1
                    fi
                    arg="${out_address}"
                else
                    arg="${arg#value:}"
                fi
                _DEVKIT_QUOTE_ARG "arg"
                arguments="${arguments:+${arguments}, }${arg}"
            done < <(_DEVKIT_DAPPFILE_CONTRACT_ARGS "${dappfile_path}" "${index}")

            PRINT "Build contract: ${contract}, account: ${account}, address: ${address}, blockchain: ${blockchain}, network: ${network}." "debug"
            PRINT "Arguments: ${arguments}" "debug"
            if ! _DEVKIT_BUILD "${projectdir}" "${deploydir}" "${finaldir}" "${blockchain}" "${network}" "${source}" "${contract}" "${account}" "${address}" "${privkey}" "${gaslimit}" "${ipc}" "${rebuild}" "${index}" "${arguments}"; then
                return 1
            fi
        else
            # Wait for builds...
            if ! _DEVKIT_BUILD_WAIT; then
                return 1
            fi
            # Successful deployment, switch directories.
            # Backup current deploydir
            if [ -d "${finaldir}" ]; then
                if true; then
                    local backupdir="${projectdir}/deploy/.${env}.${ts}"
                    mv "${finaldir}" "${backupdir}"
                else
                    # Remove it
                    rm -rf "${finaldir}"
                fi
            fi
            mv "${deploydir}" "${finaldir}"
            break
        fi
        index=$((index+1))
    done
}

#===========================
# _DEVKIT_DAPPFILE_JS
#
# Get the app confuration for js destination file.
#
# Parameters:
#   $1: Filepath of Dappfile.yaml
#
# Returns:
#   Line on stdout.
#
#===========================
_DEVKIT_DAPPFILE_JS()
{
    SPACE_SIGNATURE="dappfile_path"

    local dappfile_path="${1}"
    shift

    local data=
    data=$(cat "${dappfile_path}")

    printf "%s\\n" "${data}" | luajit -e '
lyaml=require"lyaml";
local t=lyaml.load(io.read("*all"));

function str(s, default)
    if type(s) == "table" then
        return default
    end
    return s or default
end

local js=t.app and t.app.js or {}
local lines=str(js.dest, "src/js/contracts.js")
print(lines)
'

}

#===========================
# _DEVKIT_DAPPFILE_CONTRACT_POSTDEPLOY
#
# List all post deploy entries for a contract and its associated data line by line.
#
# Parameters:
#   $1: Filepath of Dappfile.yaml
#   $2: the name of the environment we are working on
#   $3: the index of the contract to get post deployments for
#
# Returns:
#   Lines on stdout.
#
#===========================
_DEVKIT_DAPPFILE_CONTRACT_POSTDEPLOY()
{
    SPACE_SIGNATURE="dappfile_path env index"

    local dappfile_path="${1}"
    shift

    local env="${1}"
    shift

    local index="${1}"
    shift

    local data=
    data=$(cat "${dappfile_path}")

    # Disable warnings for inline Lua code
    # shellcheck disable=2086
    printf "%s\n" "${data}" | luajit -e '
lyaml=require"lyaml";
local t=lyaml.load(io.read("*all"));
local index='${index}'+1
local env="'${env}'"

function str(s)
    if type(s) == "table" then
        return ""
    end
    return s or ""
end

local Env
if #env>0 then
    Env=t._environments[env]
    if not Env or not Env.accounts then
        return
    end
end

local contract=t.contracts[index]
if not  contract then
    return
end
local a=1
local b=#(contract.postdeploy or {})
for i=a,b do
    local pd=t.contracts[index].postdeploy[i]

    local action=str(pd.action)
    local contract_name=str(pd.contract)
    local account=str(pd.account)
    local fn=str(pd.fn)
    local gas=str(pd.gas)
    local address=""
    local blockchain=""
    local network=""

    if Env then
        for k,v in ipairs(Env.accounts) do
            if v.data and v.name == account then
                blockchain, network, address = str(v.data.blockchain), str(v.data.network), str(v.data.value)
            end
        end
    end
    print(string.format("%s;%s;%s;%s;%s;%s;%s;%s",
        action, contract_name, account, address, fn, blockchain, network, gas))
end
'

}

#===========================
# _DEVKIT_DAPPFILE_CONTRACTS
#
# List all contracts and their associated data line by line.
#
# Parameters:
#   $1: Filepath of Dappfile.yaml
#   $2: the name of the environment we are working on (optional).
#   $3: the index of the contract to get, -1 means all.
#
# Returns:
#   Lines on stdout.
#
#===========================
_DEVKIT_DAPPFILE_CONTRACTS()
{
    SPACE_SIGNATURE="dappfile_path env index"

    local dappfile_path="${1}"
    shift

    local env="${1}"
    shift

    local index="${1}"
    shift

    local data=
    data=$(cat "${dappfile_path}")

    # Disable warnings for inline Lua code
    # shellcheck disable=2086
    printf "%s\n" "${data}" | luajit -e '
lyaml=require"lyaml";
local t=lyaml.load(io.read("*all"));
local index='${index}'
local env="'${env}'"

function str(s)
    if type(s) == "table" then
        return ""
    end
    return s or ""
end

local a=1
local b=#t.contracts
if index > -1 then
    a=index+1
    b=index+1
end
for i=a,b do
    local contract=t.contracts[i]
    if not contract then
        return
    end
    local name=str(contract.name)
    local source=str(contract.source)
    local account=str(contract.account)
    local gas=str(contract.gas)
    local blockchain=str(contract.blockchain or "ethereum")
    local network=str(contract.network)
    local post=contract.postdeploy and "1" or "0"
    local jsalias=contract.js and str(contract.js.alias or name)
    local nrargs=#(contract.args or {})
    local jsexport=""
    for k,v in ipairs((contract.js or {}).export or {}) do
        if v.type then
            jsexport=jsexport.." "..v.type
        end
    end

    if #env>0 then
        for k,v in ipairs(contract._environments or {}) do
            if v.data and v.name == env then
                network = str(v.data.network)
            end
        end
    end
    print(string.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s",
        name, source, account, blockchain, network, gas, post, jsalias, nrargs, jsexport))
end
'

}

#===========================
# _DEVKIT_DAPPFILE_CONTRACT_POSTDEPLOY_ARGS
#
# List all arguments for a contracts post deploys
#
# Parameters:
#   $1: Filepath of Dappfile.yaml
#   $2: Contract index
#   $3: Post Deploy index
#
# Returns:
#   Lines on stdout.
#
#===========================
_DEVKIT_DAPPFILE_CONTRACT_POSTDEPLOY_ARGS()
{
    SPACE_SIGNATURE="dappfile_path contract_index pd_index"

    local dappfile_path="${1}"
    shift

    local contract_index="${1}"
    shift

    local pd_index="${1}"
    shift

    local data=
    data=$(cat "${dappfile_path}")

    # Disable warnings for inline Lua code
    # shellcheck disable=2086
    printf "%s\n" "${data}" | luajit -e '
lyaml=require"lyaml";
local t=lyaml.load(io.read("*all"));
local contract_index='${contract_index}'+1
local pd_index='${pd_index}'+1
for k,v in ipairs(t.contracts[contract_index].postdeploy[pd_index].args or {}) do
    if v.value then
        print(string.format("value:%s", v.value))
    elseif v.contract then
        -- TODO: possibly allow for finer grained names, if we want
        -- same contract name on multiple chains in the same deployment.
        -- TODO: check contract name for illegal chars.
        print(string.format("contract:%s.*.*.*.*.addr", v.contract))
    else
        print(string.format("value:UNKNOWN"))
    end
end
'

}

#===========================
# _DEVKIT_DAPPFILE_CONTRACT_ARGS
#
# List all arguments for a contract
#
# Parameters:
#   $1: Filepath of Dappfile.yaml
#   $2: Contract index
#
# Returns:
#   Lines on stdout.
#
#===========================
_DEVKIT_DAPPFILE_CONTRACT_ARGS()
{
    SPACE_SIGNATURE="dappfile_path index"

    local dappfile_path="${1}"
    shift

    local index="${1}"
    shift

    local data=
    data=$(cat "${dappfile_path}")

    # Disable warnings for inline Lua code
    # shellcheck disable=2086
    printf "%s\n" "${data}" | luajit -e '
lyaml=require"lyaml";
local t=lyaml.load(io.read("*all"));
local index='${index}'+1

for k,v in ipairs(t.contracts[index].args or {}) do
    if v.account then
        print(string.format("account:%s", v.account))
    elseif v.value then
        print(string.format("value:%s", v.value))
    elseif v.contract then
        -- TODO: possibly allow for finer grained names, if we want
        -- same contract name on multiple chains in the same deployment.
        -- TODO: check contract name for illegal chars.
        print(string.format("contract:%s.*.*.*.*.addr", v.contract))
    else
        print(string.format("value:UNKNOWN"))
    end
end
'
}

#===========================
# _DEVKIT_BUILD
#
# Inner implementation of build
#
# Parameters:
#   $1: projects home directory
#   $2: deployment directory
#   $3: backup directory
#   $4: blockchain name
#   $5: network name
#   $6: source file
#   $7: contract file
#   $8: account
#   $9: address
#   $10: key for signing
#   $11: gas limit
#   $12: IPC address
#   $13: rebuild flag
#   $14: Contract index
#   $15: arguments
#
# Returns:
#   Non-zero on error
#
#===========================
_DEVKIT_BUILD()
{
    SPACE_SIGNATURE="projectdir deploydir backupdir blockchain network source contract account address privkey gaslimit ipc rebuild contract_index arguments"
    SPACE_DEP="PRINT _DEVKIT_BUILD_ETHEREUM_EXTERNAL"

    local projectdir="${1}"
    shift

    local deploydir="${1}"
    shift

    local backupdir="${1}"
    shift

    local blockchain="${1}"
    shift

    local network="${1}"
    shift

    local source="${1}"
    shift

    local contract="${1}"
    shift

    local account="${1}"
    shift

    local address="${1}"
    shift

    local privkey="${1}"
    shift

    local gaslimit="${1}"
    shift

    local ipc="${1}"
    shift

    local rebuild="${1}"
    shift

    local contract_index="${1}"
    shift

    local arguments="${1}"
    shift

    if [ "${blockchain}" = "ethereum" ]; then
        # TODO: get a given gasprice from env variables for a specific network.
        local gasprice="0x3B9ACA00"
        if [ "${network}" = "local" ]; then
            local host="http://localhost:8545/"
            local chainid=333
        elif [ "${network}" = "infuranet" ]; then
            local host="https://infuranet.infura.io/"
            local chainid=5810
        elif [ "${network}" = "kovan" ]; then
            local host="https://kovan.infura.io/"
            local chainid=42
        elif [ "${network}" = "mainnet" ]; then
            local host="https://mainnet.infura.io/"
            local chainid=1
        elif [ "${network}" = "ropsten" ]; then
            local host="https://ropsten.infura.io/"
            local chainid=3
        elif [ "${network}" = "rinkeby" ]; then
            local host="https://rinkeby.infura.io/"
            local chainid=4
        else
            PRINT "Unknown Ethereum network: ${network}. Hint: use lower case." "error"
            return 1
        fi
        _DEVKIT_BUILD_ETHEREUM_EXTERNAL "${projectdir}" "${deploydir}" "${backupdir}" "${source}" "${contract}" "${account}" "${address}" "${privkey}" "${ipc}" "${network}" "${host}" "${chainid}" "${gaslimit}" "${gasprice}" "${rebuild}" "${contract_index}" "${arguments}"
        return
    else
        PRINT "Unknown blockchain: ${blockchain} for contract: ${contract}. Hint: use lower case." "error"
        return 1
    fi
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#==============================
# _DEVKIT_BUILD_ETHEREUM_EXTERNAL
#
# Deploy to external Ethereum geth node via HTTP
#
# Parameters:
#   $1: projects home directory
#   $2: deployment directory
#   $3: backup directory
#   $4: source file
#   $5: contract file
#   $6: account
#   $7: address
#   $8: key for signing
#   $9: IPC address
#   $10: network
#   $11: external host address
#   $12: external network ID
#   $13: gas limit
#   $14: gas price
#   $15: rebuild flag
#   $16: Contract index
#   $17: arguments
#
# Expects:
#   GETH
#   _deploy_pending: array
#
# Returns:
#   Non-zero on error
#
#==============================
_DEVKIT_BUILD_ETHEREUM_EXTERNAL()
{
    SPACE_SIGNATURE="projectdir deploydir backupdir source contract account address privkey ipc network host chainid gaslimit gasprice rebuild contract_index arguments"
    SPACE_DEP="_DEVKIT_ETHEREUM_ADD_ARGS DEVKIT_ACCOUNT_SIGN _WEB3_JSON_EXTRACT"
    SPACE_ENV="GETH=\"${GETH:-geth}\""

    local projectdir="${1}"
    shift

    local deploydir="${1}"
    shift

    local backupdir="${1}"
    shift

    local source="${1}"
    shift

    local contract="${1}"
    shift

    local account="${1}"
    shift

    local address="${1}"
    shift

    local privkey="${1}"
    shift

    local ipc="${1}"
    shift

    local network="${1}"
    shift

    local host="${1}"
    shift

    local chainid="${1}"
    shift

    local gaslimit="${1}"
    shift

    local gasprice="${1}"
    shift

    local rebuild="${1}"
    shift

    local contract_index="${1}"
    shift

    local arguments="${1}"
    shift

    if [ -z "${privkey}" ] || [ "${privkey}" = "0x0" ]; then
        PRINT "Missing key for signing transaction" "error"
        return 1
    fi

    if ! command -v "${GETH}" >/dev/null; then
        PRINT "Unable to find geth executable at: ${GETH}" "error"
        return 1
    fi

    # External: dirname
    local contractdir=
    contractdir="${projectdir}/$(dirname "${source}")"

    local binfile="${contractdir}/${contract}.bin"
    if [ ! -f "${binfile}" ]; then
        PRINT "Could not find compiled contract: ${contract}.bin" "error"
        return 1
    fi

    local abifile="${contractdir}/${contract}.abi"
    if [ ! -f "${abifile}" ]; then
        PRINT "Could not find compiled contract ABI: ${contract}.abi" "error"
        return 1
    fi

    local contract_bin=
    contract_bin=$(cat "${binfile}")
    if [ "${contract_bin#0x}" = "${contract_bin}" ]; then
        contract_bin="0x"${contract_bin}
    fi

    local deployfile="${deploydir}/${contract}.ethereum.${network}.${account}.${address}.deploy"
    local argsfile="${deploydir}/${contract}.ethereum.${network}.${account}.${address}.args"
    local backupfile=
    if [ -n "${backupdir}" ]; then
        backupfile="${backupdir}/${contract}.ethereum.${network}.${account}.${address}.deploy"
    fi

    local contract_abi=
    contract_abi=$(cat "${abifile}")

    local deploybin=
    if ! deploybin=$(_DEVKIT_ETHEREUM_ADD_ARGS "${ipc}" "${contract_abi}" "${contract_bin}" "${arguments}"); then
        return 1
    fi

    printf "%s\\n" "${deploybin}" > "${deployfile}"
    printf "%s\\n" "${arguments}" > "${argsfile}"

    if [ "${rebuild}" != "true" ] && [ -n "${backupfile}" ]; then
        # Check if same binary already is deployed, then copy that address and be done.
        local addrsrcfile="${backupdir}/${contract}.ethereum.${network}.${account}.${address}.addr"
        if [ -f "${addrsrcfile}" ]; then
            local contractaddress=
            contractaddress=$(cat "${addrsrcfile}")
            local deployedcode=
            if ! deployedcode=$(_DEVKIT_ETHEREUM_GET_CONTRACT "${host}" "${contractaddress}"); then
                PRINT "Could not get deployed contract code at address ${contractaddress}." "error"
                return 1
            fi
            PRINT "Retrieved deployment from host: ${host} address: ${contractaddress} code: [${deployedcode}]" "debug"
            if [ -n "${deployedcode}" ]; then
                # Something is deployed at the address.
                if [ -f "${backupfile}" ]; then
                    if cmp -s "${deployfile}" "${backupfile}"; then
                        # Same binary, copy cache files and be done.
                        local addrfile="${deploydir}/${contract}.ethereum.${network}.${account}.${address}.addr"
                        local txfile="${deploydir}/${contract}.ethereum.${network}.${account}.${address}.tx"
                        local txsrcfile="${backupdir}/${contract}.ethereum.${network}.${account}.${address}.tx"
                        if [ -f "${addrsrcfile}" ]; then
                            cp "${addrsrcfile}" "${addrfile}"
                            cp "${txsrcfile}" "${txfile}"
                            PRINT "${contract} not changed, not rebuilding."
                            return
                        fi
                    fi
                fi
            fi
        fi
    fi

    #
    # Transaction data
    local __from=${address}
    #local __to="0x0000000000000000000000000000000000000000"
    local __to=""
    local __data=${deploybin}
    local __value="0x0"

    #
    # Default Gas
    if [ "${gaslimit}" = "" ] || [ -z "${gaslimit}" ] ; then
        local cmd='console.log(eth.estimateGas({"data": "'${__data}'"}));'
        local output=
        output=$(${GETH} attach "${ipc}" --exec "${cmd}" | head -1)
        if [ $? -gt 0 ]; then
            gaslimit=300000
        else
            gaslimit="${output}"
            PRINT "Gas limit has been automatically set for ${contract} to estimated value: ${gaslimit}" "info"
        fi
    fi

    #
    # Sign transaction and dispatch
    __signed_data=$(DEVKIT_ACCOUNT_SIGN "${host}" "${chainid}" "${__from}" "${__to}" "${__data}" "${__value}" "${privkey}" "${gasprice}" "${gaslimit}")
    # shellcheck disable=2086
    __transaction_data=$(curl -s -X POST -H "Content-Type: application/json" --data '{"jsonrpc": "2.0", "id": 1, "method": "eth_sendRawTransaction", "params": ["'${__signed_data}'"]}' "${host}")
    if [ $? -gt 0 ]; then
        PRINT "Could not deploy ${contract}" "error"
        return 1
    fi

    # Detect error mesage
    local error=
    if ! error=$(_WEB3_JSON_EXTRACT "${__transaction_data}" "/error/message"); then
        PRINT "Could not extract from JSON" "error"
        return 1
    elif [ -n "${error}" ] && [ "${error}" != "null" ]; then
        PRINT "Deployment of ${contract} failed: ${error}" "error"
        return 1
    fi

    local output=
    if ! output=$(_WEB3_JSON_EXTRACT "${__transaction_data}" "/result"); then
        PRINT "Could not extract from JSON for ${contract}" "error"
        return 1
    fi

    local tx=
    tx=$(printf "%s\n" "${output}")

    if [ "${tx#0x}" = "${tx}" ]; then
        PRINT "Error deploying contract ${contract}: ${tx}" "error"
        return 1
    fi
    PRINT "Pending deployment of ${contract} with tx: ${tx}" "info"
    local line="pending _DEVKIT_BUILD_ETHEREUM_CB ethereum ${network} ${contract} ${account} ${address} ${tx} ${host} ${gaslimit} ${deploydir} ${contract_index}"
    if [ "${#_deploy_pending[@]}" -gt 0 ]; then
        _deploy_pending=("${_deploy_pending[@]}" "${line}")
    else
        _deploy_pending=("${line}")
    fi
}


#===========================
# _DEVKIT_ETHEREUM_COUNT_FN_ARGS
#
# Count function arguments
#
# Parameters:
#   $1: source file
#   $2: contract file
#
# Returns:
#   Non-zero on error
#
#===========================
_DEVKIT_ETHEREUM_COUNT_FN_ARGS()
{
    SPACE_SIGNATURE="source contract"
    SPACE_DEP="PRINT"

    local file="${1}"
    shift

    local contract="${1}"
    shift

    local content=
    content="$(cat "${file}")"

    local constructor_signature=
    if [[ $content =~ [\ $'\n'$'\t']function[\ ]+"${contract}"[\ ]*\(([^\)]*)\)[^\{]*\{ ]]; then
        constructor_signature="${BASH_REMATCH[1]}"
        local num_args=0
        constructor_signature="${constructor_signature//[ ]/}"
        if [ -n "${constructor_signature}" ]; then
            constructor_signature="${constructor_signature//[!,]/}"
            num_args="${#constructor_signature}"
            num_args=$((num_args+1))
        fi
        printf "%s\\n" "${num_args}"
    else
        PRINT "No constructor found in ${contract}." "debug"
        printf "%s\\n" "0"
    fi
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#===========================
# _DEVKIT_ETHEREUM_ADD_ARGS
#
# Add arguments
#
# Parameters:
#   $1: IPC address
#   $2: ABI file
#   $3: bin file
#   $4: arguments
#
# Returns:
#   Non-zero on error
#
#===========================
_DEVKIT_ETHEREUM_ADD_ARGS()
{
    SPACE_SIGNATURE="ipc abi bin args"

    local ipc="${1}"
    shift

    local contract_abi="${1}"
    shift

    local contract_bin="${1}"
    shift

    local arguments="${1}"
    shift

    local js_cmd="
    var contract_bin='${contract_bin}';
    var contract_abi=JSON.parse('${contract_abi}');
    var contract = eth.contract(contract_abi);
    console.log(contract.new.getData(${arguments}${arguments:+, }{data: contract_bin}));
    "
    PRINT "cmd: ${js_cmd}" "debug"

    local result=
    result=$(${GETH} attach "${ipc}" --exec "${js_cmd}"  | head -1)

    if [ $? -gt 0 ]; then
        PRINT "Failed to append constructor arguments. Result: ${result}" "error"
        return 1
    fi

    printf "%s\\n" "${result}"
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#=================================
# _DEVKIT_BUILD_ETHEREUM_CB
#
# Callback function to check if a contract has been deployed.
#
# Parameters:
#   $1: line
#
# Returns:
#   Non-zero on error
#
#=================================
_DEVKIT_BUILD_ETHEREUM_CB()
{
    SPACE_SIGNATURE="line"
    SPACE_DEP="PRINT STRING_ITEM_GET _DEVKIT_ETHEREUM_GET_CONTRACT"
    SPACE_ENV="NODE=\"${NODE:-node}\""

    local line="${1}"
    shift

    local network=
    STRING_ITEM_GET "${line}" 3 "network"
    local contract=
    STRING_ITEM_GET "${line}" 4 "contract"
    local account=
    STRING_ITEM_GET "${line}" 5 "account"
    local address=
    STRING_ITEM_GET "${line}" 6 "address"
    local tx=
    STRING_ITEM_GET "${line}" 7 "tx"
    local host=
    STRING_ITEM_GET "${line}" 8 "host"
    local gas=
    STRING_ITEM_GET "${line}" 9 "gas"
    local deploydir=
    STRING_ITEM_GET "${line}" 10 "deploydir"

    local addressfile="${deploydir}/${contract}.ethereum.${network}.${account}.${address}.addr"
    local txfile="${deploydir}/${contract}.ethereum.${network}.${account}.${address}.tx"

    local receipt=
    receipt=$(${NODE} -e '
const web3 = require("web3");
const web = new web3(new web3.providers.HttpProvider("'${host}'"));
const tx_receipt = web.eth.getTransactionReceipt("'${tx}'");
if(tx_receipt && tx_receipt.contractAddress) {
    console.log("receipt:" + tx_receipt.contractAddress + " " + tx_receipt.gasUsed);
 } // this must be prefixed by a space otherwise space parser will fail.
')
    if [ $? -gt 0 ]; then
        return 1
    fi

    # Disable warning about escaped literal in regex
    # shellcheck disable=1001
    if [[ ${receipt} =~ ^receipt[\:]([^\ ]+)\ (.+)$ ]]; then
        PRINT "$receipt" "debug"
        local contractaddress="${BASH_REMATCH[1]}"
        local gasused="${BASH_REMATCH[2]}"

        # Check if contract has been successfully deployed
        local deployedcode=
        if ! deployedcode=$(_DEVKIT_ETHEREUM_GET_CONTRACT "${host}" "${contractaddress}"); then
            PRINT "Could not get deployed contract code at address ${contractaddress}." "error"
            return 1
        fi

        if [ -z "${deployedcode}" ]; then
            PRINT "Error deploying ${contract}. This could mean the contract has not been properly constructed. Missing or wrong order of constructor arguments? Too low gaslimit?" "error"
            return 1
        fi

        PRINT "Successfully deployed ${contract} to address: ${contractaddress}, gas used: ${gasused}." "info"
        printf "%s\n" "${contractaddress}" > "${addressfile}"
        printf "%s\n" "${tx}" > "${txfile}"
        return 2
    fi
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#==============================
# DEVKIT_ETHEREUM_GET_CONTRACT
#
# Return the contract binary prefixed by 0x,
# or empty string if not found.
#
# Parameters:
#   $1: host address
#   $2: contract address
#
# Expects:
#   NODE to point to node.js, defaults to "node".
#
# Returns:
#   Contract bytecode. Non-zero on error
#
#==============================
_DEVKIT_ETHEREUM_GET_CONTRACT()
{
    SPACE_SIGNATURE="host address"
    SPACE_ENV="NODE=\"${NODE:-node}\""

    local host="${1}"
    shift

    local address="${1}"
    shift

    PRINT "Retrieving contract from: ${host} address: ${address}" "debug"
    local code=
    # shellcheck disable=2086
    code=$(${NODE} -e '
const web3 = require("web3");
const web = new web3(new web3.providers.HttpProvider("'${host}'"));
const code = web.eth.getCode("'${address}'");
if(code) console.log(code);
')
    if [ $? -gt 0 ]; then
        return 1
    fi

    if [ "${code}" = "0x" ]; then
        code=""
    elif [ "${#code}" -gt 2 ]; then
        if [ "${code#0x}" = "${code}" ]; then
            code="0x${code}"
        fi
    fi

    printf "%s\\n" "${code}"
}

#=================================
# _DEVKIT_POST_DEPLOY
#
# Process post deployment
#
# Parameters:
#   $1: IPC address
#   $2: projects home directory
#   $3: project name
#   $4: environment name
#
# Expects:
#   $_deploy_done
#
# Returns:
#   Non-zero on error
#
#=================================
_DEVKIT_POST_DEPLOY()
{
    SPACE_SIGNATURE="ipc projectshome project env"
    # shellcheck disable=SC2034
    SPACE_DEP="PRINT STRING_ITEM_GET _DEVKIT_QUOTE_ARG _DEVKIT_DAPPFILE_CONTRACT_POSTDEPLOY _DEVKIT_DAPPFILE_CONTRACT_POSTDEPLOY_ARGS _DEVKIT_POST_DEPLOY2"

    local ipc="${1}"
    shift

    local projectshome="${1}"
    shift

    local project="${1}"
    shift

    local env="${1}"
    shift

    if [[ ! ${env} =~ ^([a-zA-Z0-9_]+)$ ]]; then
        return 2
    fi

    local projectdir="${projectshome}/${project}"

    if [ ! -d "${projectdir}" ]; then
        PRINT "Could not find project directory for ${project}." "error"
        return 1
    fi

    local dappfile_path="${projectdir}/Dappfile.yaml"
    if [ ! -f "${dappfile_path}" ]; then
        PRINT "No Dappfile.yaml found in project ${project}." "error"
        return 1
    fi

    local index=0
    while [ "${#_deploy_done[@]}" -gt "${index}" ]; do
        local line="${_deploy_done[${index}]}"
        local blockchain=
        STRING_ITEM_GET "${line}" 2 "blockchain"
        local network=
        STRING_ITEM_GET "${line}" 3 "network"
        local contract=
        STRING_ITEM_GET "${line}" 4 "contract"
        local account=
        STRING_ITEM_GET "${line}" 5 "account"
        local address=
        STRING_ITEM_GET "${line}" 6 "address"
        local deploydir=
        STRING_ITEM_GET "${line}" 10 "deploydir"
        local contract_index=
        STRING_ITEM_GET "${line}" 11 "contract_index"
        local index2=0
        local line2=
        while read -r line2; do
            local pd_action=
            local pd_contract=
            local pd_account=
            local pd_address=
            local pd_fn=
            STRING_ITEM_GET "${line2}" 0 "pd_action"
            local pd_contract=
            STRING_ITEM_GET "${line2}" 1 "pd_contract"
            local pd_account=
            STRING_ITEM_GET "${line2}" 2 "pd_account"
            local pd_address=
            STRING_ITEM_GET "${line2}" 3 "pd_address"
            local pd_fn=
            STRING_ITEM_GET "${line2}" 4 "pd_fn"
            local arguments=""
            local arg=
            while read -r arg; do
                if [ "${arg#contract:}" != "${arg}" ]; then
                    arg="${arg#contract:}"
                    # Argument is referring to a deployed contract address.
                    # Get the address
                    local address_file="${deploydir}/${arg}"
                    # Note: Unquoted on purpose.
                    # shellcheck disable=2086
                    if [ -f ${address_file} ]; then
                        local argv=
                        argv=$(cat ${address_file})
                        if [ -z "${argv}" ]; then
                            PRINT "Could not get anticipated address of ${arg} which is used as argument to ${contract} in post deploy." "error"
                            return 1
                        fi
                        arg="${argv}"
                    else
                        PRINT "Could not get anticipated address of ${arg} which is used as argument to ${contract} in post deploy." "error"
                        return 1
                    fi
                else
                    arg="${arg#value:}"
                fi
                _DEVKIT_QUOTE_ARG "arg"
                arguments="${arguments:+${arguments}, }${arg}"
            done < <(_DEVKIT_DAPPFILE_CONTRACT_POSTDEPLOY_ARGS "${dappfile_path}" "${contract_index}" "${index2}")
            index2=$((index2+1))
            # Post TX
            PRINT "Post deploy TX contract: ${contract}, account: ${account}, address: ${address}, blockchain: ${blockchain}, network: ${network}." "debug"
            PRINT "Arguments: ${arguments}" "debug"
            if ! _DEVKIT_POST_DEPLOY2; then
                return 1
            fi
        done < <(_DEVKIT_DAPPFILE_CONTRACT_POSTDEPLOY "${dappfile_path}" "${env}" "${contract_index}")
        index=$((index+1))
    done
    if ! _DEVKIT_WAIT_POST_DEPLOY2; then
        return 1
    fi
}

_DEVKIT_POST_DEPLOY2()
{
    # TODO
    :
}

_DEVKIT_WAIT_POST_DEPLOY2()
{
    # TODO
    :
}

#=================================
# _DEVKIT_BUILD_WAIT
#
# Wait for build step to complete
#
# Parameters:
#   None
#
# Expects:
#   $_deploy_pending
#   $_deploy_done
#
# Returns:
#   Non-zero on error
#
#=================================
_DEVKIT_BUILD_WAIT()
{
    SPACE_SIGNATURE=""
    # Note: All callback functions must be included.
    SPACE_DEP="PRINT STRING_ITEM_GET _DEVKIT_BUILD_ETHEREUM_CB"

    if [ "${#_deploy_pending[@]}" -eq 0 ]; then
        return
    fi

    PRINT "Waiting for contract(s) to be created." "info"

    while [ "${#_deploy_pending[@]}" -gt 0 ]; do
        local line="${_deploy_pending[0]}"
        local cb=
        STRING_ITEM_GET "${line}" 1 "cb"
        $cb "${line}"
        local status="$?"
        if [ "${status}" -eq 1 ]; then
            return 1
        elif [ "${status}" -eq 2 ]; then
            # Successful deploy
            if [ "${#_deploy_pending[@]}" -gt 1 ]; then
                _deploy_pending=("${_deploy_pending[@]:1}")
            else
                _deploy_pending=()
            fi
            _deploy_done+=("${line}")
            continue
        fi
        sleep 1
    done
}

#=============
# DEVKIT_JS_BUILD_PROJECT
#
# Parameters:
#   $1: projects home directory
#   $2: project name
#   $3: environment name
#   $4: wallets list
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_JS_BUILD_PROJECT()
{
    SPACE_SIGNATURE="projectshome project env wallets"
    # shellcheck disable=SC2034
    SPACE_DEP="PRINT STRING_ITEM_GET _DEVKIT_DAPPFILE_CONTRACTS _DEVKIT_DAPPFILE_JS _DEVKIT_DAPPFILE_ACCOUNT"

    local projectshome="${1}"
    shift

    local project="${1}"
    shift

    local env="${1}"
    shift

    local wallets="${1}"
    shift

    if [[ ! ${env} =~ ^([a-zA-Z0-9_]+)$ ]]; then
        return 2
    fi

    local projectdir="${projectshome}/${project}"

    if [ ! -d "${projectdir}" ]; then
        PRINT "Could not find project directory for ${project}." "error"
        return 1
    fi

    local dappfile_path="${projectdir}/Dappfile.yaml"
    if [ ! -f "${dappfile_path}" ]; then
        PRINT "No Dappfile.yaml found in project ${project}." "error"
        return 1
    fi

    local deploydir="${projectdir}/deploy/${env}"
    if [ ! -d "${deploydir}" ]; then
        mkdir -p "${deploydir}" || {
            PRINT "Can't create deploydir ${deploydir}." "error"
            return 1
        }
    fi

    local js_file=
    if ! js_file=$(_DEVKIT_DAPPFILE_JS "${dappfile_path}"); then
        PRINT "Could not find contract referenced for js. Check the Dappfile.yaml." "error"
        return 1
    fi
    if [ -z "${js_file}" ]; then
        return
    fi
    local js_code="if(!contracts) var contracts={};
"
    # For each contract in Dappfile
    while read -r line; do
        local contract=
        local source=
        local account=
        local blockchain=
        local network=
        local js=
        local jsexport=
        local _ifs="${IFS}"
        IFS=";"
        STRING_ITEM_GET "${line}" 0 "contract"
        STRING_ITEM_GET "${line}" 1 "source"
        STRING_ITEM_GET "${line}" 2 "account"
        STRING_ITEM_GET "${line}" 3 "blockchain"
        STRING_ITEM_GET "${line}" 4 "network"
        STRING_ITEM_GET "${line}" 7 "js"
        STRING_ITEM_GET "${line}" 9 "jsexport"
        IFS="${_ifs}"
        if [ -z "${jsexport}" ]; then
            continue
        fi
        local out_address=
        local out_privkey=
        if ! _DEVKIT_DAPPFILE_ACCOUNT "${dappfile_path}" "${env}" "${account}" "${blockchain}" "${wallets}"; then
            PRINT "Could not find address for account ${account}." "error"
            return 1
        fi
        local address="${out_address}"
        # External: dirname
        local contractdir=
        contractdir="${projectdir}/$(dirname "${source}")"

        js_code="${js_code}if(!contracts['$js']) contracts['$js']={blockchain: '$blockchain', network: '$network'};
"
        local arg=
        for arg in ${jsexport}; do
            # Note: This is focused on Ethereum as for now.
            if [ "${arg}" = "abi" ]; then
                local inc_file="${contractdir}/${contract}.abi"
                if [ -f "${inc_file}" ]; then
                    local abi=
                    abi=$(cat "${inc_file}")
                    js_code="${js_code}contracts['$js'].abi=$abi;
"
                else
                    PRINT "Could not find ABI file of ${contract}. Did you compile?." "error"
                    return 1
                fi
            elif [ "${arg}" = "bin" ]; then
                local inc_file="${contractdir}/${contract}.bin"
                if [ -f "${inc_file}" ]; then
                    local bin=
                    bin="0x"$(cat "${inc_file}")
                    js_code="${js_code}contracts['$js'].bin='$bin';
"
                else
                    PRINT "Could not find ABI file of ${contract}. Did you compile?." "error"
                    return 1
                fi
            elif [ "${arg}" = "address" ]; then
                # TODO: Might have to make it more fine grained if we allow same name of contract for multiple chains.
                local inc_file="${deploydir}/${contract}.*.*.*.*.addr"
                # Note: Unquoted on purpose
                # shellcheck disable=2086
                if [ -f ${inc_file} ]; then
                    local addr=
                    addr=$(cat ${inc_file})
                    js_code="${js_code}contracts['$js'].address='$addr';
"
                else
                    PRINT "Could not find address file of ${contract}. Did you deploy?." "error"
                    return 1
                fi
            elif [ "${arg}" = "account" ]; then
                js_code="${js_code}contracts['$js'].account_name='$account';
"
            elif [ "${arg}" = "account_address" ]; then
                js_code="${js_code}contracts['$js'].account_address='$address';
"
            else
                PRINT "Unknown type of js export: ${arg}." "error"
                return 1
            fi
        done
        contract_index=
        IFS='
'
    done < <(_DEVKIT_DAPPFILE_CONTRACTS "${dappfile_path}" "${env}" "-1")

    js_file="${projectdir}/${js_file}"
    if printf "%s" "${js_code}" > "${js_file}"; then
        PRINT "Successfully built js file: ${js_file}."
    else
        PRINT "Could not build js file: ${js_file}." "error"
        return 1
    fi
}

#=============
# DEVKIT_TEST
#
# Test a contract
#
# Parameters:
#   $1: projects home directory
#   $2: project name
#   $3: contract file name
#   $4: specific test name (optional)
#   $5: hot reload flag (optional)
#   $6: break on first failure flag (optional)
#
# Expects:
#   TESTER path to tester binary
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_TEST()
{
    SPACE_SIGNATURE="projectshome project contract:0 name:0 hotreload:0 breakonfailure:0"
    SPACE_DEP="PRINT"
    SPACE_ENV="TESTER=\"\${TESTER:-/home/devkit/node_modules/mocha/bin/mocha}\""

    local projectshome="${1}"
    shift

    local project="${1}"
    shift

    local files="${1:-"*.sol"}"
    shift

    local name="${1}"
    shift

    local hotreload="${1}"
    shift

    local breakonfailure="${1}"
    shift

    cd "${projectshome}/${project}" 2>/dev/null || {
        PRINT "Could not find project directory ${project}." "error"
        return 1
    }

    files="${files%.sol}.sol"
    local file=
    while true; do
        for file in ${files}; do
            if [ ! -f "${file}" ]; then
                if [ "${files}" = "*.sol" ]; then
                    # If no contracts in root dir then try in ./contracts
                    files="./contracts/*.sol"
                    continue 2;
                else
                    file="./contracts/${file}"
                fi

                if [ ! -f "${file}" ]; then
                    PRINT "Could not find file: ${file}" "error"
                    return 1
                fi
            fi

            local file_naked="${file%.*}"
            PRINT "Test ${file_naked}" "info"
            if command -v "${TESTER}" >/dev/null; then
                # Setup tester switches and options
                local flags="--slow 1 --timeout 60000"
                if [ "${hotreload}" = "true" ]; then
                    flags="${flags} --watch"
                fi
                if [ "${breakonfailure}" = "true" ]; then
                    flags="${flags} --bail"
                fi
                if [ "${name}" != "" ]; then
                    flags="${flags} --grep ${name}"
                fi
                PRINT "Flags set to: ${flags}" "debug"

                # Run tester
                local exit_status=
                # Note: intentionally missing quotes around flags
                # shellcheck disable=2086
                ${TESTER} "./tests/${file_naked}_test.js" "--node_address=http://localhost:8545" "--contract_name=${file_naked}" ${flags}
                exit_status="$?"
                if [ "${exit_status}" -gt 0 ]; then
                    return "${exit_status}"
                fi
            else
                PRINT "Unable to find tester executable at: ${TESTER}" "error"
                return 1
            fi
        done
        return 0
    done

    return 1
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#=============
# DEVKIT_GET_RECEIPT
#
# Read a transaction receipt
#
# Parameters:
#   $1: Geth node IPC endpoint
#   $2: receipt hash
#
# Expects:
#   GETH to point to go-ethereum, defaults to "geth".
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_GET_RECEIPT()
{
    SPACE_SIGNATURE="ipc receipt"
    # shellcheck disable=SC2034
    SPACE_ENV="GETH=\"${GETH:-geth}\""

    local ipc="${1}"
    shift

    local receipt="${1}"
    shift

    if ! command -v "${GETH}" >/dev/null; then
        PRINT "Unable to find geth executable at: ${GETH}" "error"
        return 1
    fi

    local cmd="eth.getTransactionReceipt('${receipt}');"
    local output=
    output=$(${GETH} attach "${ipc}" --exec "${cmd}")
    if [ $? -gt 0 ]; then
        return 1
    fi
    # We need to quote the keys of the output to make it proper JSON format.
    local json=
    json=$(printf "%s\n" "${output}" | sed 's/^\([\ ]*\)\([^\:]\+\):\(.*\)$/\1"\2":\3/')
    printf "%s\n" "${json}"
}

#=============
# DEVKIT_CONVERT_TO_ETHER
#
# Convert to ether
#
# Parameters:
#   $1: value
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_CONVERT_TO_ETHER()
{
    SPACE_SIGNATURE="value"

    local value="${1}"
    shift

    echo "scale=18; ${value}/10^18" | bc -l | sed 's/^\./0./'
}

#=============
# DEVKIT_CONVERT_TO_WEI
#
# Convert to wei
#
# Parameters:
#   $1: value
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_CONVERT_TO_WEI()
{
    # shellcheck disable=SC2034
    SPACE_SIGNATURE="value"

    local value="${1}"
    shift

    echo "${value}*10^18" | bc -l | cut -d'.' -f1
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#=============
# DEVKIT_ADD_ARGS
#
# Append arguments to existing compiled binary file
# This operation considers that arguments are always
# appended the same way and not packed differently in
# optimized builds.
#
# Parameters:
#   $1: IPC address
#   $2: projects home directory
#   $3: project name
#   $4: contract file name
#   $@: arguments
#
# Expects:
#   SOLC
#   GETH
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_ADD_ARGS()
{
    SPACE_SIGNATURE="ipc projectshome project contract:0 [arguments]"
    SPACE_DEP="PRINT _DEVKIT_QUOTE_ARG _DEVKIT_ETHEREUM_ADD_ARGS"
    # shellcheck disable=2034
    SPACE_ENV="SOLC=\"${SOLC:-solc}\" GETH=\"${GETH:-geth}\""

    local ipc="${1}"
    shift

    local projectshome="${1}"
    shift

    local project="${1}"
    shift

    local contract="${1}"
    shift

    # Append extension if needed and store as file
    local file=
    if [ "${contract}" = "${contract#*.}" ]; then
        contract="${contract}.sol"
    fi
    file="./contracts/${contract}"

    if ! command -v "${SOLC}" >/dev/null; then
        PRINT "Unable to find solc executable at: ${SOLC}" "error"
        return 1
    fi

    if ! command -v "${GETH}" >/dev/null; then
        PRINT "Unable to find geth executable at: ${GETH}" "error"
        return 1
    fi

    local arguments=""
    local arg=
    for arg in "$@"; do
        _DEVKIT_QUOTE_ARG "arg"
        arguments="${arguments:+${arguments}, }${arg}"
    done

    cd "${projectshome}/${project}" 2>/dev/null || {
        PRINT "Could not find project directory ${project}." "error"
        return 1
    }

    if [ ! -f "${file}" ]; then
        PRINT "Could not find file: ${file}" "error"
        return 1
    fi

    if [ -n "${arguments}" ]; then
        PRINT "Appending constructor arguments: ${arguments}" "debug"
        local contract_bin=
        contract_bin="0x"$(cat "${file%.*}.bin")
        local contract_abi=
        contract_abi=$(cat "${file%.*}.abi")

        local result=
        result=$(_DEVKIT_ETHEREUM_ADD_ARGS "${ipc}" "${contract_abi}" "${contract_bin}" "${arguments}")

        if [ $? -gt 0 ]; then
            PRINT "Failed to append constructor arguments. Result: ${result}" "error"
            # TODO: Cleanup on error
            return 1
        else
            printf "%s\n" "${result}"
            return 0
        fi
    fi
}

#=====================
# _DEVKIT_QUOTE_ARG
#
# Helper function for escaping and quoting arguments in place
#
# Parameters:
#   $1: variable to quote
#
# Returns:
#   Non-zero on failure
#
#=====================
_DEVKIT_QUOTE_ARG()
{
    # shellcheck disable=SC2034
    SPACE_SIGNATURE="variable"
    # shellcheck disable=SC2034
    SPACE_DEP="STRING_ESCAPE"

    local _var=
    eval "_var=\"\${${1}}\""

    if [ "${_var#\[}" != "${_var}" ]; then
        # Array, use sed to quote every hex string
        _var=$(printf "%s\n" "${_var}" | sed 's/\(0x[a-zA-Z0-9]*\)/"&"/g')
    elif [ "${_var#\{}" != "${_var}" ]; then
        # Object, do nothing
        :
    elif [ "${_var}" = "true" ]; then
        # Boolean, do nothing
        :
    elif [ "${_var}" = "false" ]; then
        # Boolean, do nothing
        :
    elif [ "${_var}" = "null" ]; then
        # Null, do nothing
        :
    elif [ "$(printf "%d" "${_var}" 2>/dev/null)" = "${_var}" ]; then
        # Integer, do nothing
        :
    elif [ "${_var#\"}" = "${_var}" ]; then
        # Unquoted string or hex value, quote it
        STRING_ESCAPE "_var" '"'
        _var="\"${_var}\""
    fi

    eval "${1}=\"\${_var}\""
}

#=====================
# DEVKIT_PARSE_DAPPFILE
#
# Parse Dapp configuration file
#
# Parameters:
#   None
#
# Returns:
#   Non-zero on failure
#
#=====================
_DEVKIT_PARSE_DAPPFILE()
{
    # shellcheck disable=SC2034
    SPACE_SIGNATURE=""
    # shellcheck disable=SC2034
    SPACE_DEP=""

    local data=
    data=$(cat ./Dappfile.yaml)
    local result=
    result=$(printf "%s\n" "${data}" | luajit -e 'lyaml=require"lyaml";local t=lyaml.load(io.read("*all"));print(t.title)')
    PRINT "result: $result" "info"
}

#=====================
# DEVKIT_MAKE_PROJECT
#
# Perform all steps needed to completely make a project
#
# Parameters:
#   $1: projects home directory
#   $2: project name
#   $3: optimization flag
#   $4: recompile flag
#   $5: IPC address
#   $6: environment name
#   $7: rebuild flag
#   $8: wallets
#
# Returns:
#   Non-zero on failure
#
#=====================
DEVKIT_MAKE_PROJECT()
{
    # shellcheck disable=SC2034
    SPACE_SIGNATURE="projectshome project optimization:0 recompile:0 ipc env rebuild wallets"
    # shellcheck disable=SC2034
    SPACE_DEP="DEVKIT_COMPILE_PROJECT DEVKIT_BUILD_PROJECT DEVKIT_JS_BUILD_PROJECT"

    DEVKIT_COMPILE_PROJECT "${1}" "${2}" "${3}" "${4}" "" &&
    DEVKIT_BUILD_PROJECT "${5}" "${1}" "${2}" "${6}" "${7}" "${8}" &&
    DEVKIT_JS_BUILD_PROJECT "${1}" "${2}" "${6}" "${8}"
}

#================
# _DEVKIT_ENV_WRAP
#
# When being run inside a container as root,
# we will try to run as a user with the same uid
# as the owner of the project on the host disk.
#
# Parameters:
#   None
#
# Returns:
#   Non-zero on error
#
#================
_DEVKIT_ENV_WRAP()
{
    SPACE_DEP="PRINT"
    # shellcheck disable=SC2034
    SPACE_ARGS="\"\${RUN}\""

    if [ "$(id -u)" = 0 ]; then
        if [ -d /home/space ]; then
            local uid=
            if uid=$(stat -c %u /home/devkit/projects); then
                if ! id -u "user${uid}" >/dev/null 2>/dev/null; then
                    if ! adduser -u "${uid}" -D "user${uid}"; then
                        PRINT "Could not create user user${uid}." "error"
                        return 254
                    fi
                    if ! addgroup "user${uid}" space; then
                        PRINT "Could not add user${uid} to group space." "error"
                        return 254
                    fi
                fi
                local path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
                local bash=
                bash=$(command -v bash)
                su -l "user${uid}" -s "${bash}" -c "export PATH=\"${path}\" NODE_PATH=/home/devkit/node_modules SPACE_MODULES_SHARED=/home/space; $@"
                return
            fi
        fi
    fi
    bash -c "$@"
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#=============
# DEVKIT_WATCH_EVENTS
#
# Watch all events from one or more contracts
#
# Parameters:
#   $1: ipc address
#   $2: ABI file
#   $3: contract address
#   $4: event name
#   $5: from block number
#   $6: to block number
#
# Expects:
#   GETH to point to go-ethereum, defaults to "geth".
#
# Returns:
#   0: success
#   1: failure
#
#=================
DEVKIT_WATCH_EVENTS()
{
    # shellcheck disable=SC2034
    SPACE_SIGNATURE="ipc abi contract_address event_name [from_block to_block]"
    # shellcheck disable=SC2034
    SPACE_ENV="GETH=\"${GETH:-geth}\""
    # shellcheck disable=SC2034
    SPACE_DEP="PRINT"

    local ipc="${1}"
    shift

    local abi="${1}"
    shift

    local contract_address="${1}"
    shift

    local event_name="${1}"
    shift

    local from_block="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    local to_block="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    if ! command -v "${GETH}" >/dev/null; then
        PRINT "Unable to find geth executable at: ${GETH}" "error"
        return 1
    fi

    local contract_abi=
    contract_abi=$(cat "${abi}")

    local cmd="
        var contract_abi=JSON.parse('${contract_abi}');
        var contract = eth.contract(contract_abi).at('${contract_address}');
        var contract_events = contract.${event_name}({}, {fromBlock: '${from_block}', toBlock: '${to_block}'});
        var events = contract_events.get(function(error, result){for(var i = 0; i < result.length ; i++){res=result[i].event + ' ' + JSON.stringify(result[i].args);console.log(res);}});
        "
    local output=
    output=$(${GETH} attach "${ipc}" --exec "${cmd}")
    if [ $? -gt 0 ]; then
        return 1
    fi
    printf "%s\n" "${output}"
}

#=====================
# WEB3_JSON_EXTRACT
#
# Handles JSON data reading
#
# Parameters:
#   $1: json file
#   $2: path
#
# Returns:
#   Non-zero on failure
#
#=====================
_WEB3_JSON_EXTRACT()
{
    SPACE_SIGNATURE="json path"
    # shellcheck disable=SC2034
    SPACE_DEP="OS_COMMAND PRINT STRING_SUBST"

    local json="${1}"
    shift

    local path="${1}"
    shift

    # Convert path to jq format
    STRING_SUBST "path" "/" "." "1"
    # Adjust for arrays
    STRING_SUBST "path" ".[" "["

    if ! OS_COMMAND jq >/dev/null; then
        PRINT "Could not find jq command to extract from json." "error"
        return 1
    fi

    printf "%s\n" "${json}" | jq -r "${path}" 2>/dev/null
}


# Disable warning about indirectly checking exit code
# shellcheck disable=SC2181

#=====================
# DEVKIT_DAPPFILE_ACCOUNT
#
# Retrieve account information contained in a given wallet
#
# Parameters:
#   $1: Dappfile path
#   $2: environment name
#   $3: account address
#   $4: blockchain name
#   $5: wallets list
#
# Expects:
#   NODE to point to node.js, defaults to "node".
#
# Returns:
#   out_address
#   out_privkey
#   Non-zero on error
#
#=====================
_DEVKIT_DAPPFILE_ACCOUNT()
{
    # shellcheck disable=SC2034
    SPACE_SIGNATURE="dappfile env account blockchain wallets"
    # shellcheck disable=SC2034
    SPACE_ENV="NODE=\"${NODE:-node}\""

    local dappfile_path="${1}"
    shift

    local env="${1}"
    shift

    local account="${1}"
    shift

    local blockchain="${1}"
    shift

    local wallets="${1}"
    shift

    if ! command -v "${NODE}" >/dev/null; then
        PRINT "Unable to find node executable at: ${NODE}" "error"
        return 1
    fi

    local data=
    data=$(cat "${dappfile_path}")

    local output
    # Disable warnings for inline Lua code
    # shellcheck disable=2086
    output=$(printf "%s\n" "${data}" | luajit -e '
lyaml=require"lyaml";
local t=lyaml.load(io.read("*all"));
local account_name="'${account}'"
local blockchain="'${blockchain}'"
local env="'${env}'"

function str(s)
    if type(s) == "table" then
        return ""
    end
    return s or ""
end

local accounts=(t.accounts or {})
local a=1
local b=#accounts
local walletname=""
local accountindex=""
for i=a,b do
    local account=accounts[i]
    if account.name == account_name and account.blockchain == blockchain then
        walletname=str(account.wallet)
        accountindex=str(account.index)
        if #env > 0 then
            for k,v in ipairs(account._environments or {}) do
                if v.data and v.name == env then
                    walletname = str(v.data.wallet or walletname)
                    accountindex = str(v.data.index or accountindex)
                end
            end

        end
        break
    end
end
print(string.format("%s;%s", walletname, accountindex))
')

    local _ifs="${IFS}"
    IFS=";"
    #local hdpath="m/44'/60'/0'/0";
    local getwalletname=
    local keyindex=
    STRING_ITEM_GET "${output}" 0 "getwalletname"
    STRING_ITEM_GET "${output}" 1 "keyindex"
    IFS="${_ifs}"

    if [ -z "${getwalletname}" ]; then
        PRINT "Could not find wallet for account ${account}." "error"
        return 1
    fi

    if [ -z "${keyindex}" ]; then
        PRINT "Could not find account index for account ${account}." "error"
        return 1
    fi

    local _ifs="${IFS}"
    IFS=";"
    local walletseed=
    local wallethdpath=
    local index=0
    while true; do
        local walletname=
        STRING_ITEM_GET "${wallets}" ${index} "walletname"
        if [ -z "${walletname}" ]; then
            break;
        fi
        if [ "${walletname}" = "${getwalletname}" ]; then
            STRING_ITEM_GET "${wallets}" $((index+1)) "walletseed"
            STRING_ITEM_GET "${wallets}" $((index+2)) "wallethdpath"
            break;
        fi
        index=$((index+3))
    done
    IFS="${_ifs}"
    if [ -z "${walletseed}" ]; then
        PRINT "Could not find wallet seed for account ${account}." "error"
        return 1
    fi
    if [ -z "${wallethdpath}" ]; then
        PRINT "Could not find wallet hdpath for account ${account}." "error"
        return 1
    fi

    local output=
    output=$(${NODE} -e '
var lightwallet = require("eth-lightwallet");
var keystore = lightwallet.keystore;

var seed="'"${walletseed}"'";
var keyindex='${keyindex}';
var hdPath= "'"${wallethdpath}"'";

var password="";
keystore.createVault({
    password: password,
    seedPhrase: seed,
    hdPathString: hdPath,
}, function (err, ks) {
    ks.keyFromPassword(password, function (err, pwDerivedKey) {
        if (err) throw err;
        ks.generateNewAddress(pwDerivedKey, keyindex+1);
        var addresses = ks.getAddresses();
        var address = addresses[keyindex];
        var privkey = ks.exportPrivateKey(address, pwDerivedKey)
        console.log(address + " " + privkey);
    });
});
')
    if [ $? -gt 0 ]; then
        return 1
    fi

    if [[ $output =~ ^([^\ ]+)\ (.+)$ ]]; then
        out_address="${BASH_REMATCH[1]}";
        out_privkey="${BASH_REMATCH[2]}";
    else
        return 1
    fi
}
